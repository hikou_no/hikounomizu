/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GameEngine.hpp"
#include "Graphics/Window.hpp"
#include "Configuration/Configuration.hpp"
#include "Engines/Resources/JoystickManager.hpp"
#include "Fight/FightRules.hpp"
#include "Structs/Character.hpp"

#include <cassert>

GameEngine::GameEngine(Window &window,
                       Configuration &configuration,
                       JoystickManager &joyManager) :
m_window(&window), m_configuration(&configuration), m_joyManager(&joyManager),
m_nextScreen(nullptr), m_mainMenu((*this)), m_options((*this)),
m_charactersMenu((*this)), m_arenaMenu((*this)), m_localGame((*this)),
m_networkedGame((*this)), m_exited(false)
{

}

//Main menu
void GameEngine::initMainMenu()
{
    setNextScreen(m_mainMenu);
}

//Options
void GameEngine::initOptions()
{
    setNextScreen(m_options);
}

//Characters menu
void GameEngine::confCharactersMenu_playersNo(std::size_t playersNo)
{
    m_charactersMenu.setPlayersNo(playersNo);
}

void GameEngine::initCharactersMenu()
{
    setNextScreen(m_charactersMenu);
}

//Arena menu
void GameEngine::initArenaMenu()
{
    setNextScreen(m_arenaMenu);
}

//Fight screen
void GameEngine::confLocalGame_arena(const std::string &arenaName)
{
    m_localGame.setArena(arenaName);
}

void GameEngine::confLocalGame_players(const std::vector<Character> &players)
{
    m_localGame.clearPlayers();

    for (const Character &player : players)
        m_localGame.addPlayer(player);
}

void GameEngine::confLocalGame_rules(const FightRules &rules)
{
    m_localGame.setRules(rules);
}

void GameEngine::initLocalGame()
{
    setNextScreen(m_localGame);
}

//Networked game screen
void GameEngine::initNetworkedGame()
{
    setNextScreen(m_networkedGame);
}

void GameEngine::setNextScreen(Screen &nextScreen)
{
    m_nextScreen = &nextScreen;
}

void GameEngine::launchNextScreen()
{
    assert(m_nextScreen != nullptr);
    m_nextScreen->run();
}

void GameEngine::exit()
{
    m_exited = true;
}

bool GameEngine::wasExited() const
{
    return m_exited;
}

//Get and set methods
Window &GameEngine::getWindow() const
{
    return (*m_window);
}

Configuration &GameEngine::getConfiguration() const
{
    return (*m_configuration);
}

JoystickManager &GameEngine::getJoystickManager() const
{
    return (*m_joyManager);
}
