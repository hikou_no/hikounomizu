/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SOUND_EFFECTS_LIST
#define DEF_SOUND_EFFECTS_LIST

#define CHARACTER_AGNOSTIC_EFFECT ""

#include <map>
#include <vector>
#include <string>
#include <random>

/// A sound path with an associated pitch variation range
struct SoundData
{
    SoundData() = default;
    SoundData(const std::string &soundPath, float pitch) :
    path(soundPath), pitchVariation(pitch) {}

    std::string path;
    float pitchVariation = .15f;
};

/// A map of SoundEffect objects,
/// separated by character name or character-agnostic
using SoundEffect = std::map<std::string, std::vector<SoundData>>;

/// A class to associate abstract sound effect names
/// with one or several character-specific sound paths
class SoundEffectsList
{
    public:
        SoundEffectsList();

        bool load(const std::string &xmlRelPath);
        bool getSoundData(const std::string &effect,
                          const std::string &characterName,
                          SoundData &target);

    private:
        bool chooseSoundAmong(const std::vector<SoundData> &sounds,
                              SoundData &target);

        std::mt19937 m_generator;
        std::map<std::string, SoundEffect> m_soundEffects;
};

#endif
