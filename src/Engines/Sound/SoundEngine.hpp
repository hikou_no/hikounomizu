/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SOUND_ENGINE
#define DEF_SOUND_ENGINE

#include "Audio/Sound.hpp"
#include "SoundEffectsList.hpp"
#include "SoundInterface.hpp"

#include <list>
#include <string>
#include <random>

class SoundBufferManager;

/// Class managing the playback of sounds effects.
/// It can store and play simultaneously an unlimited number of sounds.
class SoundEngine : public SoundInterface
{
    public:
        SoundEngine();

        void playSound(const std::string &soundPath, float pitch=1.f,
                       float volumeFactor=1.f) override;

        void playSoundEffect(const std::string &effect,
                             const std::string &character,
                             float volumeFactor=1.f) override;

        void playSoundEffectFixedPitch(const std::string &effect,
                                       const std::string &character,
                                       float volumeFactor=1.f) override;

        void update();
        void loadSoundEffects(const std::string &effectsPath);

        void setManager(SoundBufferManager &manager);
        void setVolume(float volume);

    private:
        std::mt19937 m_generator;
        std::uniform_real_distribution<float> m_pitchDist;

        std::list<Sound> m_playingSounds;
        float m_volume;

        SoundEffectsList m_effects;
        SoundBufferManager *m_manager;
};

#endif
