/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_AMBIENT_PLAYER
#define DEF_AMBIENT_PLAYER

#include "Ambient.hpp"
#include <thread>
#include <mutex>

class AmbientPlayer : public Ambient
{
    public:
        explicit AmbientPlayer(float ambientVolume);
        AmbientPlayer(const AmbientPlayer &copied) = delete;
        AmbientPlayer &operator=(const AmbientPlayer &copied) = delete;
        ~AmbientPlayer();

        void play(const std::string &ambientName) override;
        void stop() override final;

    private:
        void threadMain(std::string ambientName, float ambientVolume);

        std::thread *m_thread; ///< Thread for ambient playing in the background
        std::mutex m_mutex; ///< Mutex to protect accesses to m_stop
        float m_ambientVolume; ///< The wanted volume for the ambient music

        //Shared data
        bool m_stop; ///< Whether to stop the ambient music playing thread
};

#endif
