/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SOUND_INTERFACE
#define DEF_SOUND_INTERFACE

#include <string>

/// Interface to play sound effects, implemented by SoundEngine on the client
class SoundInterface
{
    public:
        virtual ~SoundInterface() = default;

        /// Plays a sound file with optional pitch and gain adjustments
        virtual void playSound(const std::string &soundPath, float pitch=1.f,
                               float volumeFactor=1.f) = 0;

        /// Plays a named sound effect with a random pitch variation
        virtual void playSoundEffect(const std::string &effect,
                                     const std::string &character,
                                     float volumeFactor=1.f) = 0;

        /// Plays a named sound effect with no pitch variation (pitch = 1.f)
        virtual void playSoundEffectFixedPitch(const std::string &effect,
                                               const std::string &character,
                                               float volumeFactor=1.f) = 0;
};

#endif
