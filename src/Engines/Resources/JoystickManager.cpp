/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "JoystickManager.hpp"
#include "Tools/Input/Joystick.hpp"
#include "Structs/JoystickData.hpp"

#include "Tools/Log.hpp"
#include "Tools/Format.hpp"

JoystickManager::~JoystickManager()
{
    free();
}

void JoystickManager::free()
{
    std::map<int, Joystick*>::iterator it;
    for (it = m_joysticksList.begin(); it != m_joysticksList.end(); ++it)
        delete it->second;

    m_joysticksList.clear();
}

int JoystickManager::loadJoystick(int joyIndex)
{
    //Loading joystick.
    SDL_JoystickEventState(SDL_ENABLE);

    Joystick *joystick = new Joystick();
    if (!joystick->load(joyIndex))
    {
        delete joystick;
        return JOYSTICK_INSTANCE_INVALID;
    }

    int instanceID = joystick->getInstanceID();

    //Joystick already loaded.
    if (isLoaded(instanceID))
    {
        delete joystick;
        return JOYSTICK_INSTANCE_INVALID;
    }

    m_joysticksList[instanceID] = joystick;

    Log::info(Format::format("Loaded joystick: {} ({})",
                             joystick->getName(), instanceID));

    return instanceID;
}

void JoystickManager::removeJoystick(int joyInstanceID)
{
    if (isLoaded(joyInstanceID))
    {
        Log::info(Format::format("Removed joystick: {} ({})",
                                 m_joysticksList[joyInstanceID]->getName(),
                                 joyInstanceID));

        delete m_joysticksList[joyInstanceID];
        m_joysticksList.erase(joyInstanceID);
    }
}

bool JoystickManager::isLoaded(int joyInstanceID) const
{
    std::map<int, Joystick*>::const_iterator it;
    it = m_joysticksList.find(joyInstanceID);

    if (it != m_joysticksList.end())
        return true;

    return false;
}

JoystickData JoystickManager::getJoystickData(int joyInstanceID) const
{
    std::map<int, Joystick*>::const_iterator it;
    it = m_joysticksList.find(joyInstanceID);

    if (it != m_joysticksList.end())
    {
        return JoystickData(joyInstanceID,
                            it->second->getName(),
                            PlayerKeys::defaultKeys((*it->second)));
    }

    return JoystickData(joyInstanceID, "undefined",
                        PlayerKeys::undefinedKeys());
}

std::vector<JoystickData> JoystickManager::getLoadedJoysticksData() const
{
    std::vector<JoystickData> joystickData;

    std::map<int, Joystick*>::const_iterator it;
    for (it = m_joysticksList.begin(); it != m_joysticksList.end(); ++it)
    {
        joystickData.emplace_back(it->first,
                                  it->second->getName(),
                                  PlayerKeys::defaultKeys((*it->second)));
    }

    return joystickData;
}

void JoystickManager::loadJoysticks()
{
    for (int i = 0; i < SDL_NumJoysticks(); i++)
        loadJoystick(i);
}
