/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TextureManager.hpp"

TextureManager::~TextureManager()
{
    std::map<std::string, Texture*>::iterator it;
    for (it = m_texturesList.begin(); it != m_texturesList.end(); ++it)
        delete it->second;
}

Texture &TextureManager::getTexture(const std::string &texturePath,
                                    Texture::MinMagFilter filter)
{
    Texture *foundTexture = findLoadedTexture(texturePath);

    if (foundTexture == nullptr)
        return loadTexture(texturePath, filter);

    return (*foundTexture);
}

Texture *TextureManager::findLoadedTexture(const std::string &texturePath)
{
    std::map<std::string, Texture*>::iterator it;
    it = m_texturesList.find(texturePath);

    if (it != m_texturesList.end())
        return it->second;

    return nullptr;
}

Texture &TextureManager::loadTexture(const std::string &texturePath,
                                     Texture::MinMagFilter filter)
{
    Texture *texture = new Texture();
    texture->loadFromFile(texturePath, filter);

    m_texturesList[texturePath] = texture;

    return (*texture);
}
