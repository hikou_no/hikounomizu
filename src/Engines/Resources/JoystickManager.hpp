/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_JOYSTICK_MANAGER
#define DEF_JOYSTICK_MANAGER

#include <vector>
#include <map>
#include <string>

class Joystick;
struct JoystickData;

/// Class loading and keeping track of requested joysticks
/// to load them only once
class JoystickManager
{
    public:
        JoystickManager() = default;
        JoystickManager(const JoystickManager &copied) = delete;
        JoystickManager &operator=(const JoystickManager &copied) = delete;
        ~JoystickManager();
        void free();

        int loadJoystick(int joyIndex);
        void removeJoystick(int joyInstanceID);
        bool isLoaded(int joyInstanceID) const;

        JoystickData getJoystickData(int joyInstanceID) const;
        std::vector<JoystickData> getLoadedJoysticksData() const;

        void loadJoysticks();

    private:
        std::map<int, Joystick*> m_joysticksList;
};

#endif
