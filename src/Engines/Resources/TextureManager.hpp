/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_TEXTURE_MANAGER
#define DEF_TEXTURE_MANAGER

#include "Graphics/Resources/Texture.hpp"

#include <map>
#include <string>

/// Class loading and keeping track of requested textures
/// to load them only once
class TextureManager
{
    public:
        TextureManager() = default;
        TextureManager(const TextureManager &copied) = delete;
        TextureManager &operator=(const TextureManager &copied) = delete;
        ~TextureManager();

        Texture &getTexture(const std::string &texturePath,
            Texture::MinMagFilter filter=Texture::MinMagFilter::Linear);

    private:
        Texture *findLoadedTexture(const std::string &texturePath);
        Texture &loadTexture(const std::string &texturePath,
                             Texture::MinMagFilter filter);

        std::map<std::string, Texture*> m_texturesList;
};

#endif
