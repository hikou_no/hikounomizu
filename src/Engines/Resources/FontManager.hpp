/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_FONT_MANAGER
#define DEF_FONT_MANAGER

#include "Graphics/Resources/Font.hpp"

#include <map>
#include <string>

/// Class loading and keeping track of requested fonts
/// to load them only once.
class FontManager
{
    public:
        FontManager();
        FontManager(const FontManager &copied) = delete;
        FontManager &operator=(const FontManager &copied) = delete;
        ~FontManager();

        Font &getDisplay(int fontSize); ///< Shortcut to get the display font
        Font &getDefault(int fontSize); ///< Shortcut to get the default font

    private:
        Font &getFont(const std::string &path, int fontSize);

        Font *findLoadedFont(const std::string &path, int fontSize);
        Font &loadFont(const std::string &path, int fontSize);

        std::map<std::pair<std::string, int>, Font*> m_fontsList;
        FT_Library m_library;
};

#endif
