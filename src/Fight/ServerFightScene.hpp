/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SERVER_FIGHT_SCENE
#define DEF_SERVER_FIGHT_SCENE

#include "Player/Control/Remote/NetServerController.hpp"
#include "FightScene.hpp"
#include <list>
#include <string>
#include <cstddef>

class PhysicsObjectStatus;
class EtherStatus;

#ifdef HNMSERVER_DRAW
    #include "Graphics/Drawable/ArenaDrawer.hpp"
    #include "Graphics/Drawable/PlayerDrawer.hpp"
    #include "Graphics/Drawable/WeaponEtherDrawer.hpp"
    class TextureManager;
#endif

class ServerFightScene : public FightScene
{
    public:
        #ifdef HNMSERVER_DRAW
            explicit ServerFightScene(TextureManager &textureManager);
            void draw();
        #else
            ServerFightScene();
        #endif

        void setArena(const std::string &arenaName) override;

        ServerPlayerInterface *addNetServerPlayer(const std::string &name);
        bool removePlayer(std::size_t playerIx);

        void revivePlayers(); ///< Revive ko players

        std::vector<PhysicsObjectStatus> getPlatformStatus() const;
        const std::list<TimedDamage> &getQueuedDamages() const;
        EtherStatus getWeaponEtherStatus() const;

    private:
        void updatePlayerControllers() override;

        std::list<NetServerController> m_netControl;

        #ifdef HNMSERVER_DRAW
            TextureManager *m_textureManager;
            ArenaDrawer m_arenaDrawer;
            std::vector<PlayerDrawer> m_playerDrawers;
            WeaponEtherDrawer m_weaponsDrawer;
        #endif
};

#endif
