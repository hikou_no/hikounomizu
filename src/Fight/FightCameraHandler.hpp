/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_FIGHT_CAMERA_HANDLER
#define DEF_FIGHT_CAMERA_HANDLER

#include "Tools/Timer.hpp"
#include "Structs/Box.hpp"
#include "Graphics/Viewport.hpp"
#include <vector>
#include <list>

class Player;

/// Fighting scene camera handler, smoothly optimizes view while
/// ensuring that every player is always visible
class FightCameraHandler
{
    public:
        enum class CameraMode { Static, Dynamic };

        FightCameraHandler();

        void setArena(const Box &arenaBox, float groundLevel);
        void setPlayers(const std::vector<const Player*> &players);
        void setMode(CameraMode mode);

        void shake();
        void look(float viewWidth, float viewHeight,
                  float frameTime, float arenaMinCameraWidth);

        /// Returns the currently displayed fraction
        /// of the world, i.e., m_currentBox
        const Box &getCurrentView() const;

    private:
        void computeView(Box &view, float minWidth) const;

        /// Outputs the smallest encompassing box containing every known player
        void emcompass(Box &box) const;
        void extend(Box &box, float minWidth) const;
        void adjust(Box &box) const;

        /// Avoids the box \p box to be outside the arena box
        /// as much as possible. (As long as the ratio of the arena
        /// and of the box are alike, this should never happen)
        void center(Box &box) const;

        std::vector<const Player*> m_players;

        /// The latest known exact camera box, emcompassing all players
        Box m_targetBox;
        /// The current camera box, continuously fades
        /// to smoothly match m_targetBox
        Box m_currentBox;

        float m_arenaRatio; ///< Width / Height ratio of the arena
        Box m_arenaBox;
        float m_groundLevel; ///< Ground level relative to the top of the arena

        CameraMode m_mode;

        /// Beginning time of each current camera shake
        std::list<Timer::TimePoint> m_shakeTimes;
};

#endif
