/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_MUSIC
#define DEF_MUSIC

#include "Audio/AL.hpp"
#include <vorbis/vorbisfile.h>
#include <cstdio>
#include <string>
#include <chrono>

class Music
{
    public:
        explicit Music(const std::string &relPath);
        Music(const Music &copied) = delete;
        Music &operator=(const Music &copied) = delete;
        ~Music();

        void update();

        void play();
        void stop();

        //Get and set methods
        void setLooping(bool loop);
        void setVolume(float volume);

        bool isOpened() const; ///< Whether the music file successfully opened
        bool isPlaying() const;

        std::chrono::duration<double> getPlaybackTime() const;
        std::chrono::duration<double> getDuration() const;

    private:
        bool loadBuffer(ALuint buffer);

        bool m_opened;
        OggVorbis_File m_oggFile;
        ALuint m_alSource; ///< Sound source
        ALuint m_alBuffers[2]; ///< Streaming buffers

        ALenum m_format;
        ALsizei m_frequency;

        /// Full duration of the music
        std::chrono::duration<double> m_duration;

        /// Offset of the currently playing buffers in the music
        /// Add the current AL_SEC_OFFSET to get the exact playback time
        std::chrono::duration<double> m_playbackBase;

        float m_volume;
        bool m_loop;
};

#endif
