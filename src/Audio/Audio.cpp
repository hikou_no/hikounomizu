/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Audio.hpp"

#include "Audio/AL.hpp"

namespace Audio
{
    bool init()
    {
        ALCdevice *device = alcOpenDevice(nullptr);
        if (device == nullptr)
            return false;

        ALCcontext *context = alcCreateContext(device, nullptr);
        if (context == nullptr)
            return false;

        ALCboolean result = alcMakeContextCurrent(context);
        return (result == ALC_TRUE);
    }

    void close()
    {
        ALCcontext *context = alcGetCurrentContext();
        ALCdevice *device = alcGetContextsDevice(context);

        alcMakeContextCurrent(nullptr);

        alcDestroyContext(context);
        alcCloseDevice(device);
    }
}
