/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SOUND
#define DEF_SOUND

#include "Audio/AL.hpp"

class SoundBuffer; ///< To pass the actual sound data to be played back

class Sound
{
    public:
        Sound();
        Sound(const Sound &copied) = delete;
        Sound &operator=(const Sound &copied) = delete;
        ~Sound();

        void play();
        void stop();

        bool isPlaying() const;

        void setBuffer(SoundBuffer &buffer);
        void setVolume(float volume);
        void setPitch(float pitch);

    private:
        ALuint m_alSource;
        SoundBuffer *m_buffer;
        float m_volume, m_pitch;
};

#endif
