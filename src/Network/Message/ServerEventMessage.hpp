/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SERVER_EVENT_MESSAGE
#define DEF_SERVER_EVENT_MESSAGE

#include "Message.hpp"
#include "Network/Serialization/ServerEvent.hpp"

/// Contains an informative server event to be sent to clients for logging
class ServerEventMessage : public Message
{
    public:
        ServerEventMessage() = default;
        explicit ServerEventMessage(const WelcomeEvent &welcome);
        explicit ServerEventMessage(const RulesEvent &rules);
        explicit ServerEventMessage(const PeerConnectedEvent &peerConnected);
        explicit ServerEventMessage(const PeerDisconnectedEvent &peerDisconnected);
        explicit ServerEventMessage(const PeerInactiveEvent &peerInactive);
        explicit ServerEventMessage(const MatchmakingEvent &matchmaking);
        explicit ServerEventMessage(const GameScheduledEvent &gameScheduled);

        bool fromBuffer(const Message::Buffer &buffer) override;

        const ServerEvent &getServerEvent() const;
        Message::Buffer allocBuffer() const override;

    private:
        ServerEvent m_event;
};

#endif
