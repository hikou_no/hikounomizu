/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYER_ACTION_MESSAGE
#define DEF_PLAYER_ACTION_MESSAGE

#include "Message.hpp"
#include "Network/Serialization/PlayerAction.hpp"

/// Contains a player action that a client requests the server to perform
class PlayerActionMessage : public Message
{
    public:
        PlayerActionMessage();
        explicit PlayerActionMessage(const PlayerAction::Index &actionIndex);
        bool fromBuffer(const Message::Buffer &buffer) override;

        const PlayerAction &getPlayerAction() const;
        Message::Buffer allocBuffer() const override;

    private:
        PlayerAction m_action;
};

#endif
