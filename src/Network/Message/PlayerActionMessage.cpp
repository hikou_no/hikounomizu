/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PlayerActionMessage.hpp"

PlayerActionMessage::PlayerActionMessage() : Message() {}

PlayerActionMessage::PlayerActionMessage(const PlayerAction::Index &actionIndex) :
Message(), m_action(actionIndex)
{

}

bool PlayerActionMessage::fromBuffer(const Message::Buffer &buffer)
{
    std::list<std::uint8_t> bytes = buffer.bytes();
    return m_action.readFromBuffer(bytes);
}

Message::Buffer PlayerActionMessage::allocBuffer() const
{
    std::list<std::uint8_t> bytes;

    if (!m_action.writeToBuffer(bytes))
        return Message::Buffer();

    return Message::Buffer::alloc(bytes);
}

const PlayerAction &PlayerActionMessage::getPlayerAction() const
{
    return m_action;
}
