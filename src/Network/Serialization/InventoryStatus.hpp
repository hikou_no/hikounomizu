/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_INVENTORY_STATUS
#define DEF_INVENTORY_STATUS

#include "Weapon/WeaponType.hpp"
#include <list>
#include <cstdint>

class Player;

/// Describes the current status of a player's weapon inventory,
/// used in networked games to exchange player information
class InventoryStatus
{
    public:
        InventoryStatus();

        /// Read from Player
        void readFromPlayer(const Player &player);

        /// Read/write from buffer
        bool readFromBuffer(std::list<std::uint8_t> &bytes);
        bool writeToBuffer(std::list<std::uint8_t> &dst) const;

        unsigned int getRemaining(WeaponType weaponType) const;

    private:
        unsigned int m_shurikens;
};

#endif
