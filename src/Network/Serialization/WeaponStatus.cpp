/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "WeaponStatus.hpp"
#include "Weapon/Weapon.hpp"

#include "Tools/ByteTools.hpp"

WeaponStatus::WeaponStatus() : PhysicsObjectStatus(),
m_weaponType(WeaponType::Shuriken), m_weaponId(0)
{

}

void WeaponStatus::readFromWeapon(WeaponType type, unsigned int id,
                                  const Weapon &weapon)
{
    PhysicsObjectStatus::readFromObject(weapon);

    //Type & identifier
    m_weaponType = type;
    m_weaponId = id;
}

void WeaponStatus::applyToWeapon(Weapon &weapon) const
{
    weapon.applyPhysicsStatus((*this));
}

bool WeaponStatus::readFromBuffer(std::list<std::uint8_t> &bytes)
{
    if (bytes.empty() ||
        bytes.front() > static_cast<std::uint8_t>(WeaponType::Shuriken))
        return false;

    m_weaponType = static_cast<WeaponType>(bytes.front());
    bytes.pop_front();

    return (ByteTools::readUnsigned<unsigned int, 2>(bytes, m_weaponId) &&
            PhysicsObjectStatus::readFromBuffer(bytes));
}

bool WeaponStatus::writeToBuffer(std::list<std::uint8_t> &dst) const
{
    dst.push_back(static_cast<std::uint8_t>(m_weaponType));
    ByteTools::writeUnsigned<unsigned int, 2>(m_weaponId, dst);

    return PhysicsObjectStatus::writeToBuffer(dst);
}

WeaponType WeaponStatus::getType() const
{
    return m_weaponType;
}

unsigned int WeaponStatus::getIdentifier() const
{
    return m_weaponId;
}
