/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_EVENT_STATUS
#define DEF_EVENT_STATUS

#include "Player/PlayerEvents.hpp"
#include <list>
#include <vector>
#include <cstdint>

/// Serializes a perfect blocking event
class PerfectBlockSerializer
{
    public:
        PerfectBlockSerializer() = default;
        explicit PerfectBlockSerializer(const PerfectBlockingEvent &event);

        bool readFromBuffer(std::list<std::uint8_t> &bytes);
        bool writeToBuffer(std::list<std::uint8_t> &dst) const;

        PerfectBlockingEvent getEvent() const;

    private:
        PerfectBlockingEvent m_event;
};

/// Describes the current status of a player's events (PlayerEvents),
/// used in networked games to exchange player information
class EventStatus
{
    public:
        /// Read from player events
        void readFromEvents(const PlayerEvents &events);

        /// Apply the events to a PlayerEvents
        void applyToEvents(PlayerEvents &events) const;

        /// Read/write from buffer
        bool readFromBuffer(std::list<std::uint8_t> &bytes);
        bool writeToBuffer(std::list<std::uint8_t> &dst) const;

    private:
        std::vector<PerfectBlockSerializer> m_perfectBlocks;
};

#endif
