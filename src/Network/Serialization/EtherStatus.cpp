/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "EtherStatus.hpp"
#include "Weapon/Weapon.hpp"

#include "Tools/ByteTools.hpp"

void EtherStatus::readFromEther(
    const std::map<unsigned int, Weapon*> &etherWeapons)
{
    m_weapons.clear();
    m_weapons.reserve(etherWeapons.size());

    std::map<unsigned int, Weapon*>::const_iterator it;
    for (it = etherWeapons.begin(); it != etherWeapons.end(); ++it)
        m_weapons.push_back(it->second->getStatus());
}

bool EtherStatus::readFromBuffer(std::list<std::uint8_t> &bytes)
{
    return ByteTools::readList<WeaponStatus>(bytes, m_weapons);
}

bool EtherStatus::writeToBuffer(std::list<std::uint8_t> &dst) const
{
    return ByteTools::writeList<WeaponStatus>(m_weapons, dst);
}

const std::vector<WeaponStatus> &EtherStatus::getActiveWeapons() const
{
    return m_weapons;
}
