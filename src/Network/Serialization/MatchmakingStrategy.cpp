/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "MatchmakingStrategy.hpp"

#include "Tools/Log.hpp"

namespace
{
    const std::string randomStrategy("random");
    const std::string stackStrategy("stack");
    const std::string challengerStrategy("challenger");
}

namespace MatchmakingStrategy
{
    void writeStrategy(const Type &strategy, std::list<std::uint8_t> &dst)
    {
        dst.push_back(static_cast<std::uint8_t>(strategy));
    }

    bool readStrategy(std::list<std::uint8_t> &bytes, Type &dst)
    {
        if (bytes.empty() ||
            bytes.front() > static_cast<std::uint8_t>(Type::Challenger))
            return false;

        dst = static_cast<Type>(bytes.front());
        bytes.pop_front();

        return true;
    }

    bool isValid(const std::string &strategyValue)
    {
        return (strategyValue == randomStrategy ||
                strategyValue == stackStrategy ||
                strategyValue == challengerStrategy);
    }

    std::string getValue(Type strategy)
    {
        if (strategy == Type::Stack)
            return stackStrategy;
        else if (strategy == Type::Challenger)
            return challengerStrategy;

        return randomStrategy;
    }

    Type getStrategy(const std::string &strategyValue)
    {
        if (strategyValue == stackStrategy)
            return Type::Stack;
        else if (strategyValue == challengerStrategy)
            return Type::Challenger;

        return Type::Random;
    }
}
