/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PHYSICS_OBJECT_STATUS
#define DEF_PHYSICS_OBJECT_STATUS

#include "Structs/Vector.hpp"
#include <cstdint>
#include <list>

class PhysicsObject;

/// Describes the current status of a physics object
/// used in networked games as a basis to exchange
/// platform/weapon/player information.
class PhysicsObjectStatus
{
    public:
        virtual ~PhysicsObjectStatus() = default;

        /// Read from a physics object
        void readFromObject(const PhysicsObject &object);

        /// Read/write from buffer
        virtual bool readFromBuffer(std::list<std::uint8_t> &bytes);
        virtual bool writeToBuffer(std::list<std::uint8_t> &dst) const;

        const Vector &getPosition() const;
        const Vector &getVelocity() const;

    private:
        Vector m_position, m_velocity;
};

#endif
