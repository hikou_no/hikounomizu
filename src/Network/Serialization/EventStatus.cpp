/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "EventStatus.hpp"

#include "Structs/Vector.hpp"
#include "Tools/ByteTools.hpp"

////////////////////////////////////
///PerfectBlockSerializer///
////////////////////////////////////
PerfectBlockSerializer::PerfectBlockSerializer(
    const PerfectBlockingEvent &event) : m_event(event) {}

bool PerfectBlockSerializer::readFromBuffer(std::list<std::uint8_t> &bytes)
{
    std::uint8_t id;
    Vector origin, size;
    if (!ByteTools::readUnsigned(bytes, id) ||
        !ByteTools::readVector(bytes, origin) ||
        !ByteTools::readVector(bytes, size))
        return false;

    m_event.id = id;
    m_event.damageBox = Box(origin.x, origin.y, size.x, size.y);
    return true;
}

bool PerfectBlockSerializer::writeToBuffer(std::list<std::uint8_t> &dst) const
{
    ByteTools::writeUnsigned(m_event.id, dst);
    ByteTools::writeVector(
        Vector(m_event.damageBox.left, m_event.damageBox.top), dst);
    ByteTools::writeVector(
        Vector(m_event.damageBox.width, m_event.damageBox.height), dst);
    return true;
}

PerfectBlockingEvent PerfectBlockSerializer::getEvent() const
{
    return m_event;
}

/////////////////
///EventStatus///
/////////////////
void EventStatus::readFromEvents(const PlayerEvents &events)
{
    const std::list<PerfectBlockingEvent> &blocks = events.getPerfectBlocks();
    m_perfectBlocks = std::vector<PerfectBlockSerializer>(
        blocks.begin(), blocks.end());
}

void EventStatus::applyToEvents(PlayerEvents &events) const
{
    for (const PerfectBlockSerializer &block : m_perfectBlocks)
        events.addPerfectBlock(block.getEvent().id, block.getEvent().damageBox);
}

bool EventStatus::readFromBuffer(std::list<std::uint8_t> &bytes)
{
    return ByteTools::readList(bytes, m_perfectBlocks);
}

bool EventStatus::writeToBuffer(std::list<std::uint8_t> &dst) const
{
    return ByteTools::writeList(m_perfectBlocks, dst);
}
