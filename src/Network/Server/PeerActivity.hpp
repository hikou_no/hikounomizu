/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PEER_ACTIVITY
#define DEF_PEER_ACTIVITY

#include "Tools/Timer.hpp"
#include <chrono>
#include <vector>
#include <map>
#include <set>
#include <cstdint>

class PeerList;

/// Keeps track of the activity/inactivity of playing peers to
/// eventually kick inactive players
class PeerActivity
{
    public:
        /// Increase the inactivity time of players by \p duration,
        /// except those in \p immunePeers.
        /// Typically, KO players are not subject to inactivity
        void update(const std::chrono::seconds &duration,
                    const std::set<std::uint16_t> &immunePeers);

        /// Update the list of peers for which to keep track of activity
        /// from the players and spectators connected in \p peerList
        /// The inactivity of new players is reset to \p resetValue
        void updatePeerList(const PeerList &peerList,
                            const std::chrono::seconds &resetValue);

        /// Report an activity for the peer \p peerID
        /// (resets the inactivity duration to \p resetValue)
        void reportActivity(std::uint16_t peerID,
                            const std::chrono::seconds &resetValue);

        /// Returns a list peers that have been inactive for longer than
        /// \p timeout and should be disconnected from the server
        std::vector<std::uint16_t> getInactivePeers(
            const std::chrono::seconds &timeout) const;

    private:
        /// Peer index associated with its current duration of inactivity
        std::map<std::uint16_t, std::chrono::seconds> m_activity;
};

#endif
