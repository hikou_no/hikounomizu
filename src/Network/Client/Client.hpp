/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CLIENT
#define DEF_CLIENT

#include "Network/Host/ClientHost.hpp"
#include "ClientLobby.hpp"
#include "ClientGame.hpp"
#include "Tools/Timer.hpp"
#include "Structs/Vector.hpp"

class JoystickManager;
class TextureManager;
class FontManager;
class SoundEngine;
class Viewport;

class Client : public ClientHost::ConnectionReceiver,
               public ClientLobbyCallbackReceiver
{
    public:
        enum class Phase { Disconnected, Connecting, Connected, Disconnecting };

        Client(Configuration &conf, const Viewport &viewport,
               JoystickManager &joystickManager,
               TextureManager &textureManager,
               FontManager &fontManager,
               SoundEngine &soundEngine);

        bool init();

        /// Send a disconnection notice immediately to the server
        /// in preparation for imminent destruction
        void disconnectNow();

        void serverConnectedCallback() override;
        void serverDisconnectingCallback(KickReason reason) override;
        void serverDisconnectedCallback(KickReason reason) override;

        void lobbyConnectionRequestedCallback(const Address &address) override;
        void lobbyConnectionCancelledCallback() override;
        void lobbyExitRequestedCallback() override;

        void mouseMove(const Vector &viewCoords);
        void mouseRelease(const Vector &viewCoords);
        void keyEvent(const SDL_Event &event);

        void update(float frameTime);
        void draw(float frameTime);

        /// Return whether the client is requesting exiting
        /// and returning to the main menu
        bool willExit() const;

    private:
        void switchPhase(Phase phase);
        bool connect(const Address &address);

        ClientHost m_host;

        /// Time at which the last connection or disconnection request was
        /// sent to the client (useful to force disconnect after a timeout)
        Timer::TimePoint m_lastHostRequestTime;

        Phase m_phase;

        ClientLobby m_clientLobby;
        ClientGame m_clientGame;
        bool m_exited;
};

#endif
