/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ClientPauseTab.hpp"
#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Sound/SoundEngine.hpp"

#include "Tools/ScaledPixel.hpp"
#include "Tools/I18n.hpp" ///< _ macro for i18n

ClientPauseTab::ClientPauseTab(Font &headerFont, Font &textFont,
                               TextureManager &manager,
                               SoundEngine &soundEngine,
                               float viewWidth) :
Tab(ScaledPixel{viewWidth}(530.f),
    ScaledPixel{viewWidth}(230.f)), m_headerText(3.f),
m_choice(ClientPauseTab::Result::NoChoice), m_active(false)
{
    const ScaledPixel sp(viewWidth);

    const Sprite ui_default(manager.getTexture("gfx/ui/ui.png"),
        Box(266.f / 512.f, 0.f, 246.f / 512.f, 71.f / 512.f));
    const Sprite ui_hover(manager.getTexture("gfx/ui/ui.png"),
        Box(266.f / 512.f, 75.f / 512.f, 246.f / 512.f, 71.f / 512.f));

    SkinBox skin(sp(240.f), sp(60.f), ui_default, ui_hover);
    SkinSound sounds(soundEngine, "audio/ui/menuFocus.ogg",
                     "audio/ui/menuClick.ogg");

    m_headerText.setText(_(NetworkedGamePauseTitle));
    m_headerText.setFont(headerFont);
    m_headerText.setColor(Color(255, 255, 255), Color(50, 93, 151));
    m_headerText.setPosition((getWidth() - m_headerText.getWidth()) / 2.f,
                             sp(10.f));

    //Resume button
    m_resume.setText(_(PauseResume));
    m_resume.setTextFont(textFont);
    m_resume.setTextColor(Color(255, 255, 255));

    m_resume.setSkin(skin);
    m_resume.setSoundSkin(sounds);
    m_resume.setPosition((getWidth() - m_resume.getWidth()) / 2.f, sp(90.f));

    //Exit button
    m_disconnect.setText(_(NetworkedGamePauseDisconnect));
    m_disconnect.setTextFont(textFont);
    m_disconnect.setTextColor(Color(255, 255, 255));

    m_disconnect.setSkin(skin);
    m_disconnect.setSoundSkin(sounds);
    m_disconnect.setPosition((getWidth() - m_disconnect.getWidth()) / 2.f,
        m_resume.getYPosition() + m_resume.getHeight() + sp(10.f));
}

void ClientPauseTab::draw()
{
    if (m_active)
    {
        //Update matrix
        Drawable::pushMatrix();
        Drawable::updateMatrix();

        m_background.draw();

        m_headerText.draw();
        m_resume.draw();
        m_disconnect.draw();

        Drawable::popMatrix();
    }
}

bool ClientPauseTab::mouseRelease(const Vector &mouseCoords)
{
    if (!m_active)
        return false;

    Vector localMouse = Widget::localCoords(mouseCoords);

    if (!chosen())
    {
        if (m_resume.mouseRelease(localMouse))
            m_choice = ClientPauseTab::Result::Resume;
        else if (m_disconnect.mouseRelease(localMouse))
            m_choice = ClientPauseTab::Result::Disconnect;
    }

    return Widget::contains(localMouse);
}


bool ClientPauseTab::mouseClick(const Vector &mouseCoords)
{
    if (!m_active)
        return false;

    Vector localMouse = Widget::localCoords(mouseCoords);
    return Widget::contains(localMouse);
}

bool ClientPauseTab::mouseMove(const Vector &mouseCoords)
{
    if (!m_active)
        return false;

    Vector localMouse = Widget::localCoords(mouseCoords);

    m_resume.mouseMove(localMouse);
    m_disconnect.mouseMove(localMouse);

    return Widget::contains(localMouse);
}

void ClientPauseTab::setActive(bool active)
{
    m_active = active;
    if (!m_active)
    {
        m_choice = ClientPauseTab::Result::NoChoice;
        m_resume.setFocus(false);
        m_disconnect.setFocus(false);
    }
}

bool ClientPauseTab::active() const
{
    return m_active;
}

bool ClientPauseTab::chosen() const
{
    return (m_choice != ClientPauseTab::Result::NoChoice);
}

ClientPauseTab::Result ClientPauseTab::result() const
{
    return m_choice;
}
