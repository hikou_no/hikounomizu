/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CHANNEL
#define DEF_CHANNEL

#include <cstdint>

enum class Channel : std::uint8_t
{
    /// Channel dedicated to handshake between server and client
    /// (Reliable)
    Handshake = 0,

    /// Channel dedicated to the frequent exchange of game state
    /// (Unreliable, with occasional reliable packets containing game metadata)
    GameState = 1,

    /// Channel dedicated to the exchange of informative server events
    /// (Reliable)
    ServerEvent = 2,

    /// Channel dedicated to the transmission of player actions to the server
    /// (Reliable)
    PlayerAction = 3,

    /// Channel dedicated to periodically transmit the ping of playing peers
    /// (Unreliable)
    Ping = 4
};

namespace ChannelInfo
{
    /// Stores the number of channels to allocate
    enum : std::uint8_t { Count = 5 };

    /// Utility to return whether reliable or unreliable packets should be sent
    /// in a specific channel
    constexpr bool isReliable(Channel channel)
    {
        return (channel == Channel::Handshake ||
                channel == Channel::ServerEvent ||
                channel == Channel::PlayerAction);
    }

    /// Utility to return whether a channel ID is valid
    /// and can safely be casted to a Channel
    constexpr bool isValid(std::uint8_t channelID)
    {
        return (channelID < Count);
    }
}

#endif
