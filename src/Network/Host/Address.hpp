/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ADDRESS
#define DEF_ADDRESS

#include <enet/enet.h>
#include <string>
#include <cstdint>

class Address
{
    friend class ServerHost;
    friend class ClientHost;

    public:
        Address();
        explicit Address(const ENetAddress &address);
        Address(enet_uint32 host, enet_uint16 port);
        Address(const std::string &host, enet_uint16 port);

        bool setHost(const std::string &hostName);
        bool setPort(const std::string &port);

        std::string getHostIP() const;
        std::uint16_t getPort() const;

        /// Converts \p port to a std::uint16_t if possible, otherwise returns 0
        static std::uint16_t readPort(const std::string &port);

    private:
        ENetAddress m_enetAddr;
};

#endif
