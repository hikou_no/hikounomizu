/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ShadowText.hpp"
#include "Graphics/Resources/Font.hpp"
#include "Graphics/Resources/Texture.hpp"

ShadowText::ShadowText(float shadowSize) : Text(), m_shadowSize(shadowSize) {}

void ShadowText::draw()
{
    if (m_font == nullptr)
        return;

    Texture &texture = m_font->getTexture();
    updateGLArrays();

    if (!m_glyphs.empty())
    {
        //Update matrix
        Drawable::pushMatrix();
        Drawable::updateMatrix();

        texture.draw(m_vertices, m_texCoords, m_colors);

        Drawable::popMatrix();
    }
}

void ShadowText::setColor(const Color &color, const Color &shadowColor)
{
    Text::setColor(color);
    m_shadowColor = shadowColor;
    m_glArraysGenerated = false;
}

void ShadowText::updateGLArrays()
{
    if (m_glArraysGenerated)
        return;

    m_vertices.clear();
    m_texCoords.clear();
    m_colors.clear();

    if (m_glyphs.empty() || m_font == nullptr)
        return;

    const GLsizei texWidth = m_font->getTexture().getWidth();
    const GLsizei texHeight = m_font->getTexture().getHeight();

    //Reserve space for 20 coordinates (4 coords per glyph, 4 shadows + 1 text)
    //Each coord is 2-dimensional (x, y) -> 40 floats
    m_vertices.reserve(40 * m_glyphs.size());
    m_texCoords.reserve(40 * m_glyphs.size());

    //Each color is 4-dimensional (r, g, b, a) -> 80 bytes
    m_colors.reserve(80 * m_glyphs.size());

    float xOffset = 0.0, yOffset = m_originHeight;

    for (const GlyphKerning &glyphData : m_glyphs)
    {
        const GlyphEntry &glyph = glyphData.glyph;
        xOffset += glyphData.kerning;

        //Vertex coordinates
        const float x = xOffset + glyph.topLeft.x;
        const float y = yOffset - glyph.topLeft.y;

        std::vector<float> topLeft =
            vertexCoords(x - m_shadowSize, y - m_shadowSize, glyph.box);
        std::vector<float> topRight =
            vertexCoords(x + m_shadowSize, y - m_shadowSize, glyph.box);
        std::vector<float> bottomRight =
            vertexCoords(x + m_shadowSize, y + m_shadowSize, glyph.box);
        std::vector<float> bottomLeft =
            vertexCoords(x - m_shadowSize, y + m_shadowSize, glyph.box);
        std::vector<float> center = vertexCoords(x, y, glyph.box);

        m_vertices.insert(m_vertices.end(), topLeft.begin(), topLeft.end());
        m_vertices.insert(m_vertices.end(), topRight.begin(), topRight.end());
        m_vertices.insert(m_vertices.end(),
            bottomRight.begin(), bottomRight.end());
        m_vertices.insert(m_vertices.end(),
            bottomLeft.begin(), bottomLeft.end());
        m_vertices.insert(m_vertices.end(), center.begin(), center.end());

        //Texture coordinates
        const std::vector<float> glyphTexCoords =
            textureCoords(glyph.box, texWidth, texHeight);

        for (int i = 0; i < 5; i++) //Four shadows plus the text
            m_texCoords.insert(m_texCoords.end(),
                glyphTexCoords.begin(), glyphTexCoords.end());

        //Color values
        for (int i = 0; i < 16 /* 4 vertices * 4 shadows */; i++)
            m_colors.insert(m_colors.end(),
                {
                    m_shadowColor.red,
                    m_shadowColor.green,
                    m_shadowColor.blue,
                    m_shadowColor.alpha
                });

        for (int i = 0; i < 4 /* 4 vertices */; i++)
            m_colors.insert(m_colors.end(),
                { m_color.red, m_color.green, m_color.blue, m_color.alpha });

        xOffset += glyph.advance.x;
        yOffset += glyph.advance.y;
    }

    m_glArraysGenerated = true;
}