/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_LOBBY_CHARACTER
#define DEF_LOBBY_CHARACTER

#include "GUI/Widget/Tab.hpp"
#include "GUI/Widget/Button.hpp"
#include "GUI/CharacterPreview.hpp"
#include "Graphics/Drawable/Polygon.hpp"
#include <vector>
#include <string>
#include <cstddef>

class TextureManager;
class SoundEngine;

/// Character selector shown in the networked game lobby: Shows the currently
/// selected character and two arrows to change the selection
class LobbyCharacter : public Tab
{
    public:
        LobbyCharacter(const std::vector<std::string> &characterNames,
                       const std::string &initialCharacter,
                       TextureManager &textureManager,
                       SoundEngine &soundEngine,
                       float viewWidth);

        void draw() override;
        void update(float frameTime);

        bool mouseRelease(const Vector &mouseCoords) override;
        bool mouseClick(const Vector &mouseCoords) override;
        bool mouseMove(const Vector &mouseCoords) override;

        std::string getCharacter() const;

    private:
        void updatePreviewOrigin(CharacterPreview &preview);

        /// A pair associating a character name with its preview
        struct CharacterItem
        {
            std::string name;
            CharacterPreview preview;
        };

        Button m_leftArrow, m_rightArrow;
        std::vector<CharacterItem> m_characters;
        std::size_t m_characterIx;
};

#endif
