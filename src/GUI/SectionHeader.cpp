/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SectionHeader.hpp"
#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Resources/FontManager.hpp"
#include "Engines/Sound/SoundEngine.hpp"
#include "Tools/ScaledPixel.hpp"
#include "Tools/I18n.hpp" ///< _ macro for i18n
#include "Structs/Vector.hpp"

SectionHeader::SectionHeader(const std::string &title,
                             const std::string &subtitle,
                             TextureManager &textureManager,
                             FontManager &fontManager,
                             SoundEngine &soundEngine,
                             const Vector &viewSize) :
m_viewWidth(viewSize.x),
m_title(title, fontManager.getDisplay(ScaledPixel{m_viewWidth}(54))),
m_subtitle(subtitle, fontManager.getDisplay(ScaledPixel{m_viewWidth}(22)))
{
    const ScaledPixel sp(m_viewWidth);

    //Titles
    m_title.setColor(Color(255, 255, 255));
    m_title.setPosition(viewSize.x * .02f, sp(10.f));

    m_subtitle.setColor(Color(222, 222, 222));
    m_subtitle.setPosition(viewSize.x * .98f - m_subtitle.getWidth(),
        m_title.getYPosition() + m_title.getOriginHeight() + sp(20.f));

    //Back button
    SkinBox button_skin(sp(220.f), sp(50.f));
    button_skin.inactive = Sprite(textureManager.getTexture("gfx/ui/ui.png",
                                                Texture::MinMagFilter::Nearest),
        Box(0.f, 0.f, 240.f / 512.f, 60.f / 512.f));
    button_skin.focused = Sprite(textureManager.getTexture("gfx/ui/ui.png"),
        Box(0.f, 64.f / 512.f, 240.f / 512.f, 60.f / 512.f));

    m_backButton = TextButton(_(HeaderBack), button_skin,
        SkinSound(soundEngine, "audio/ui/menuFocus.ogg", {}));
    m_backButton.setTextFont(fontManager.getDefault(sp(24)));
    m_backButton.setTextColor(Color(255, 255, 255));
    m_backButton.setPosition(
        viewSize.x - m_backButton.getWidth() - sp(10.f), sp(10.f));

    //Background
    m_background = Polygon::rectangle(viewSize.x,
        m_subtitle.getYPosition() + m_subtitle.getHeight() + sp(5.f),
        Color(50, 93, 151), Color(0, 0, 0));
    m_background.setBorderSize(0.f);
}

void SectionHeader::draw()
{
    m_background.draw();
    m_title.draw();
    m_subtitle.draw();
    m_backButton.draw();
}

bool SectionHeader::backRelease(const Vector &mouseCoords)
{
    return m_backButton.mouseRelease(mouseCoords);
}

bool SectionHeader::backMove(const Vector &mouseCoords)
{
    return m_backButton.mouseMove(mouseCoords);
}

float SectionHeader::getHeight() const
{
    return m_subtitle.getYPosition() + m_subtitle.getHeight() +
        ScaledPixel{m_viewWidth}(5.f);
}
