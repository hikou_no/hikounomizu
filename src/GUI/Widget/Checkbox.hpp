/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CHECKBOX
#define DEF_CHECKBOX

#include "Widget.hpp"
#include "Graphics/Drawable/Polygon.hpp"
#include <string>

class SoundEngine;

class Checkbox : public Widget
{
    public:
        Checkbox();
        Checkbox(bool checked, float boxSize, float borderSize);
        void draw() override;

        bool mouseRelease(const Vector &mouseCoords) override;
        bool mouseClick(const Vector &mouseCoords) override;
        bool mouseMove(const Vector &mouseCoords) override;
        void setFocus(bool focused) override;

        void check(bool checked);
        bool isChecked() const;

        void setSoundEngine(SoundEngine &soundEngine);
        void setClickSound(const std::string &clickSound);

        float getWidth() const override;
        float getHeight() const override;

    private:
        void updateBox();

        Polygon m_checkbox;
        bool m_checked;

        float m_size;
        Color m_color;

        SoundEngine *m_soundEngine; ///< To handle sounds on events
        std::string m_clickSound;
};

#endif
