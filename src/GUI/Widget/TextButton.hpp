/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_TEXT_BUTTON
#define DEF_TEXT_BUTTON

#include "Button.hpp"
#include "Graphics/Drawable/Sprite.hpp"
#include "Graphics/Drawable/Text.hpp"

struct TextButtonSkin
{
    TextButtonSkin() : font(nullptr) {}

    TextButtonSkin(Font &textFont, const Color &color,
                   const SkinBox &skinBox, const SkinSound &skinSound) :
    font(&textFont), textColor(color), skin(skinBox), sounds(skinSound)
    {

    }

    Font *font;
    Color textColor;
    SkinBox skin;
    SkinSound sounds;
};

class TextButton : public Button
{
    public:
        explicit TextButton(const SkinBox &skin = SkinBox(),
                            const SkinSound &sounds = SkinSound());
        explicit TextButton(const std::string &text,
                            const SkinBox &skin = SkinBox(),
                            const SkinSound &sounds = SkinSound());
        TextButton(const std::string &text, const TextButtonSkin &completeSkin);

        void draw() override;

        void setSkin(const SkinBox &skin) override;
        void setFocus(bool focused) override;

        void setAvatar(const Sprite &avatar);

        void setText(const std::string &text);
        const std::string &getText() const;

        void setTextFont(Font &textFont);
        void setTextColor(const Color &textColor);

    private:
        void expandSkin();
        void centerText();

        Sprite m_avatar;
        Text m_textField;
};

#endif
