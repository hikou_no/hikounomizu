/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_TAB
#define DEF_TAB

#include "Widget.hpp"
#include "Graphics/Drawable/Polygon.hpp"

/// Abstract class representing a fixed-size frame.
/// Used as a container for other widgets (menus, option buttons...)
/// in order to build more complex interfaces (options, end of a fight...)
class Tab : public Widget
{
    public:
        Tab();
        Tab(float width, float height, const Color &color = Color(0, 0, 0, 0));
        void draw() override;

        float getWidth() const override;
        float getHeight() const override;

    protected:
        Polygon m_background;
        float m_width, m_height;
};

#endif
