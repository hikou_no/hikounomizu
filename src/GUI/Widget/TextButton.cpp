/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TextButton.hpp"

TextButton::TextButton(const SkinBox &skin, const SkinSound &sounds) :
Button(skin, sounds)
{

}

TextButton::TextButton(const std::string &text,
                       const SkinBox &skin, const SkinSound &sounds) :
TextButton(skin, sounds)
{
    setText(text);
}

TextButton::TextButton(const std::string &text,
                       const TextButtonSkin &completeSkin) :
TextButton(text, completeSkin.skin, completeSkin.sounds)
{
    if (completeSkin.font != nullptr)
        setTextFont(*(completeSkin.font));

    setTextColor(completeSkin.textColor);
}

void TextButton::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();

    if (m_selected) m_skin.selected.draw();
    else if (m_focused) m_skin.focused.draw();
    else m_skin.inactive.draw();

    m_avatar.draw();
    m_textField.draw();

    Drawable::popMatrix();
}

void TextButton::setFocus(bool focused)
{
    if (m_focused != focused)
    {
        Button::setFocus(focused);
        centerText();
    }
}

void TextButton::setSkin(const SkinBox &skin)
{
    Button::setSkin(skin);
    expandSkin();
    centerText();
}

void TextButton::centerText()
{
    const float skinWidth = getWidth(), textWidth = m_textField.getWidth();

    if (skinWidth <= 0.f || textWidth <= 0.f)
        return;

    //Center text
    const float skinHeight = getHeight();
    const float textHeight = m_textField.getOriginHeight();
    const float textScale = m_textField.getXScale();

    m_textField.setXPosition((skinWidth - textWidth*textScale) / 2.f);
    m_textField.setYPosition(skinHeight * .64f - textHeight*textScale);
}

void TextButton::expandSkin()
{
    constexpr float skinMargin = 1.2f;
    if (m_skin.box.width < m_textField.getWidth() * skinMargin)
    {
        SkinBox expandedSkin = m_skin;
        expandedSkin.box.width = m_textField.getWidth() * skinMargin;

        Button::setSkin(expandedSkin);
    }
}

void TextButton::setAvatar(const Sprite &avatar)
{
    m_avatar = avatar;

    const float avatarHeight = m_avatar.getHeight();
    const float buttonHeight = getHeight();

    if (avatarHeight > 0.f && avatarHeight > buttonHeight * .7f)
        m_avatar.setScale(buttonHeight * .7f / avatarHeight);

    m_avatar.setPosition(getWidth() * .03f,
        (buttonHeight - avatarHeight * m_avatar.getYScale()) / 2.f);
}

void TextButton::setText(const std::string &text)
{
    m_textField.setText(text);
    expandSkin();
    centerText();
}

const std::string &TextButton::getText() const
{
    return m_textField.getText();
}

void TextButton::setTextFont(Font &textFont)
{
    m_textField.setFont(textFont);
    expandSkin();
    centerText();
}

void TextButton::setTextColor(const Color &textColor)
{
    m_textField.setColor(textColor);
}
