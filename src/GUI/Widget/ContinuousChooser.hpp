/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CONTINUOUS_CHOOSER
#define DEF_CONTINUOUS_CHOOSER

#include "Widget.hpp"
#include "Graphics/Drawable/Polygon.hpp"
#include "Graphics/Drawable/Text.hpp"

struct ContinuousChooserSkin
{
    ContinuousChooserSkin() :
    font(nullptr), barSize(Vector(100.f, 1.f)), handleSize(Vector(8.f, 20.f)),
    borderSize(2.f), handleColor(Color(27, 66, 119)), hitMargin(10.f)
    {

    }

    ContinuousChooserSkin(Font &textFont, const Color &label) :
    font(&textFont), barSize(Vector(100.f, 1.f)), handleSize(Vector(8.f, 20.f)),
    borderSize(2.f), handleColor(Color(27, 66, 119)), labelColor(label),
    hitMargin(10.f)
    {

    }

    Font *font; ///< Font for text value
    Vector barSize, handleSize;
    float borderSize;
    Color handleColor, disabledHandleColor, labelColor;
    /// How many pixels to allow outside of the bar and still catch click events
    float hitMargin;
};


class ContinuousChooser : public Widget
{
    public:
        ContinuousChooser();
        ContinuousChooser(const ContinuousChooserSkin &skin, float startValue,
                          float endValue, float initialValue);
        void draw() override;

        bool mouseRelease(const Vector &mouseCoords) override;
        bool mouseClick(const Vector &mouseCoords) override;
        bool mouseMove(const Vector &mouseCoords) override;

        void setEnabled(bool enabled);
        float getValue() const;

        float getWidth() const override;
        float getHeight() const override;

    private:
        void updateValue(float localXMouse);

        Polygon m_bar, m_handle;
        Text m_valueText;

        ContinuousChooserSkin m_skin;
        float m_startValue, m_endValue, m_value;
        bool m_enabled;
};

#endif
