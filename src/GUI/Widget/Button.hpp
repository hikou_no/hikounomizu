/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_BUTTON
#define DEF_BUTTON

#include "Widget.hpp"
#include "Graphics/Drawable/Sprite.hpp"
#include <string>

class SoundEngine;

struct SkinBox
{
    SkinBox() : SkinBox(100.f, 50.f) {}

    SkinBox(float width, float height) :
    SkinBox(width, height, Sprite(), Sprite()) {}

    SkinBox(float width, float height, const Sprite &inactive,
            const Sprite &focused) :
    SkinBox(width, height, inactive, focused, Sprite()) {}

    SkinBox(float width, float height, const Sprite &inactive,
            const Sprite &focused, const Sprite &selected) :
    box( Box(0.f, 0.f, width, height) ), inactive(inactive),
    focused(focused), selected(selected)
    {

    }

    Box box;
    Sprite inactive, focused, selected;
};

struct SkinSound
{
    SkinSound() : engine(nullptr) {}

    SkinSound(SoundEngine &soundEngine, const std::string &focusSound,
              const std::string &clickSound) :
    engine(&soundEngine), focus(focusSound), click(clickSound)
    {

    }

    SoundEngine *engine; ///< To handle sounds on events
    std::string focus, click;
};

class Button : public Widget
{
    public:
        explicit Button(const SkinBox &skin = SkinBox(),
                        const SkinSound &sounds = SkinSound());
        void draw() override;

        bool mouseRelease(const Vector &mouseCoords) override;
        bool mouseClick(const Vector &mouseCoords) override;
        bool mouseMove(const Vector &mouseCoords) override;

        void select(bool select);
        bool isSelected() const;

        virtual void setSkin(const SkinBox &skin);
        void setSoundSkin(const SkinSound &soundSkin);

        float getWidth() const override;
        float getHeight() const override;

    protected:
        SkinBox m_skin;
        SkinSound m_sounds;

        bool m_selected;
};

#endif
