/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CHOOSER
#define DEF_CHOOSER

#include "Widget.hpp"
#include "TextButton.hpp"
#include "Graphics/Drawable/Sprite.hpp"
#include <cstddef>
#include <vector>

class Chooser : public Widget
{
    public:
        Chooser();
        explicit Chooser(const std::vector<std::string> &data,
                         std::size_t selection = 0);
        Chooser(const TextButtonSkin &skin,
                const std::vector<std::string> &data,
                std::size_t selection = 0);
        void draw() override;

        bool mouseRelease(const Vector &mouseCoords) override;
        bool mouseClick(const Vector &mouseCoords) override;
        bool mouseMove(const Vector &mouseCoords) override;

        /// Set the choices of the chooser, text-only
        void setData(const std::vector<std::string> &data,
                     std::size_t selection = 0);

        /// Set the choices of the chooser, text with avatars
        void setIllustratedData(
            const std::vector<std::pair<Sprite, std::string>> &illustrated,
            std::size_t selection = 0);

        void setSkin(const TextButtonSkin &skin);
        const TextButtonSkin &getExpandedSkin() const;

        void setSelection(std::size_t selection);
        std::size_t getSelection() const;

        std::string getSelectionStr() const;
        bool isOpened() const;

        float getWidth() const override;
        float getHeight() const override;

    private:
        TextButtonSkin m_choiceSkin;
        std::vector<TextButton> m_choicesList;

        std::size_t m_selection;
        bool m_opened;
};

#endif
