/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_TEXT_INPUT
#define DEF_TEXT_INPUT

#include "Widget.hpp"
#include "Graphics/Drawable/Polygon.hpp"
#include "Graphics/Drawable/Text.hpp"
#include "Tools/Timer.hpp"
#include "Tools/Utf8.hpp" ///< utf8_string
#include <SDL2/SDL.h>
#include <cstddef>

class Font;

class TextInput : public Widget
{
    public:
        enum class Mode { Visible, AlphaNumPunct, Digits };

        TextInput(Mode mode, std::size_t lengthLimit, float minWidth,
                  const std::string &initialText,
                  const Utf8::utf8_string &previewText,
                  Font &font, float viewWidth);

        void draw() override;

        bool mouseRelease(const Vector &mouseCoords) override;
        bool mouseClick(const Vector &mouseCoords) override;
        bool mouseMove(const Vector &mouseCoords) override;

        bool keyEvent(const SDL_Event &event);

        const Utf8::utf8_string &getInput() const;

        float getWidth() const override;
        float getHeight() const override;

    private:
        void appendText(const std::string &str, Utf8::utf8_string &target);
        void updateText();
        void updateTextPosition();
        void setTextFont(Font &font);

        Polygon m_background, m_cursor, m_compositionLine;
        Text m_textField, m_previewText;

        Utf8::utf8_string m_input; ///< Current validated input text
        Utf8::utf8_string m_composition; ///< Input text being composed

        Mode m_mode;
        std::size_t m_lengthLimit;

        Timer m_blinkTimer;

        float m_width;
        const float m_minWidth, m_height, m_xOffset;

        /// Thickness of the cursor and composition lines
        const float m_lineSize;
        const float m_borderSize;

        const float m_blinkPeriod;
        const Color m_cursorColor;
};

#endif
