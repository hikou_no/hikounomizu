/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SHADOW_TEXT
#define DEF_SHADOW_TEXT

#include "Graphics/GL.hpp"
#include "Graphics/Drawable/Text.hpp"

/// ShadowText: Text with shadow border effects
/// to print, e.g., character names, usernames
class ShadowText : public Text
{
    public:
        explicit ShadowText(float shadowSize = 1.f);
        void draw() override;

        void setColor(const Color &color, const Color &shadowColor);

    private:
        void updateGLArrays() override;

        float m_shadowSize;
        Color m_shadowColor;

        std::vector<GLubyte> m_colors;
};

#endif
