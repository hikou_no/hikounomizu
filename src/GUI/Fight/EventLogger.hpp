/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_EVENT_LOGGER
#define DEF_EVENT_LOGGER

#include "GUI/ShadowText.hpp"
#include "Tools/Timer.hpp"
#include <list>

class FontManager;

/// The text field containing an event text,
/// and the time the text was added
struct EventEntry
{
    EventEntry() : time(Timer::getTime()) {}

    ShadowText text;
    Timer::TimePoint time;
};

/// Graphical logger that prints networked game events as they are received
/// from the server, e.g., other peer connection or disconnection
class EventLogger : public Drawable
{
    public:
        EventLogger(FontManager &fontManager, float viewWidth);
        void draw() override;
        void update();

        void clear();
        void push(const std::string &event);

    private:
        void updatePositions();

        std::list<EventEntry> m_eventEntries;
        Font *m_font;

        const float m_viewWidth;
};

#endif
