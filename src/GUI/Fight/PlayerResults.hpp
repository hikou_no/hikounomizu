/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYER_RESULTS
#define DEF_PLAYER_RESULTS

#include "Graphics/Drawable/Polygon.hpp"
#include <vector>
#include <cstdint>
#include <cstddef>

/// Horizontal line of round results, quickly showing
/// the amount of rounds that were won by a player,
/// and showing its progress towards winning the whole game
class PlayerResults : public Drawable
{
    public:
        PlayerResults(float width, float diskDiameter, float diskBorderSize);
        void draw() override;

        void reset(std::uint8_t roundsToWin, std::uint8_t wonRounds = 0);

        /// The associated player has won a round, check a round result
        void addRoundWin();

        float getWidth() const;
        float getHeight() const;

    private:
        /// Graphical element used to represent the result of a round:
        /// - The background shown alone represents a round not yet won
        /// - The element is filled to represent a won round
        class RoundResultDisk : public Drawable
        {
            public:
                RoundResultDisk(float diskDiameter, float diskBorderSize);
                void draw() override;

                void setToWon(); ///< Set the round to won state and checks the circle

            private:
                Polygon m_background; ///< Background disk, always shown
                Polygon m_fill; ///< Inner disk, shown to represent a won round

                bool m_wonRound;
        };

        /// Round result items, an item is filled at every new registered win
        std::vector<RoundResultDisk> m_results;
        std::size_t m_winCount; ///< Number of won rounds to display

        const float m_width, m_diskDiameter, m_diskBorderSize;
};

#endif
