/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYER_EVENT_BANNER
#define DEF_PLAYER_EVENT_BANNER

#include "Graphics/Drawable/Polygon.hpp"
#include "Graphics/Drawable/Text.hpp"

class PlayerEvents;
class Font;

/// Banner displayed in the player info interface
/// when a player event occured, e.g., perfect block
class PlayerEventBanner : public Drawable
{
    public:
        explicit PlayerEventBanner(Font &fon);
        void draw() override;
        void update(const PlayerEvents &events);

    private:
        Text m_text;
        Polygon m_background;
        bool m_visible = false;
};

#endif
