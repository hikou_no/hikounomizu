/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ROUND_ANNOUNCER
#define DEF_ROUND_ANNOUNCER

#include "GUI/ShadowText.hpp"
#include "Tools/Timer.hpp"
#include "Structs/Vector.hpp"
#include <vector>
#include <string>
#include <cstdint>

class SoundEngine;
class Font;
class FightRules;
struct Color;

/// Announcer to show messages at the start of a round (Ready/Round n, Fight!)
class RoundAnnouncer
{
    public:
        explicit RoundAnnouncer(const Vector &viewSize);
        void draw();

        void clear(); ///< Clears any ongoing announcement
        void announceRound(unsigned int roundId,
                           const FightRules &rules,
                           const std::vector<std::uint8_t> &winsPerPlayer);
        void announceFight();

        void setSoundEngine(SoundEngine &soundEngine);
        void setSkin(Font &font, const Color &color, const Color &borderColor);

    private:
        void announce(const std::string &message, float duration_ms);
        void updatePosition();

        SoundEngine *m_soundEngine;

        ShadowText m_text;
        Timer m_timer;
        float m_duration_ms;

        const Vector m_viewSize;
};

#endif
