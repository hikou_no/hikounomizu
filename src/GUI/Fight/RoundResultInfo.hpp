/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ROUND_RESULT_INFO
#define DEF_ROUND_RESULT_INFO

#include "PlayerResults.hpp"
#include "GUI/ShadowText.hpp"
#include "Graphics/Drawable/Sprite.hpp"
#include "Tools/Utf8.hpp"
#include <string>
#include <cstdint>

class Font;
class TextureManager;

/// Show information about a round/game winner,
/// or informs that the round ended in a draw
class RoundResultInfo : public Drawable
{
    public:
        explicit RoundResultInfo(const Vector &viewSize);
        void draw() override;

        void setResources(Font &font, TextureManager &textureManager);

        /// Activate the winner information for a networked player win
        void activate(const Utf8::utf8_string &winnerUsername,
                      const std::string &character,
                      std::uint8_t wonRounds,
                      std::uint8_t roundsToWin);

        /// Activate the winner information for a local player win
        void activate(const std::size_t &winnerId,
                      const std::string &character,
                      std::uint8_t wonRounds,
                      std::uint8_t roundsToWin);

        void activateDraw(); ///< Activate when the round was undecided
        void deactivate();

        void positionToCenter();

        float getWidth() const;

        /// Returns y-axis position of the text plus its height
        float getTextBottomLine() const;

    private:
        void activateWinner(const std::string &winText
                             /**< Winner description text */,
                            const std::string &character
                             /**< Character name for the icon */,
                            std::uint8_t wonRounds
                             /**< Number of rounds won by the player */,
                            std::uint8_t roundsToWin
                             /**< Number of rounds required to win the game */);

        enum class Status { Deactivated, PlayerWin, DrawGame };

        ShadowText m_text; ///< Text describing the result

        Sprite m_icon; ///< Icon showing the winning character, if any
        /// Show the overall number of won rounds by the player, if any winner
        PlayerResults m_results;

        TextureManager *m_textureManager;
        const Vector m_viewSize;

        Status m_status;
};

#endif
