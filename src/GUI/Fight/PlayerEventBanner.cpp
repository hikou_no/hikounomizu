/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PlayerEventBanner.hpp"
#include "Player/PlayerEvents.hpp"
#include "Graphics/Resources/Font.hpp"

#include "Tools/I18n.hpp" ///< _ macro for i18n

PlayerEventBanner::PlayerEventBanner(Font &font) : Drawable()
{
    m_text.setFont(font);
    m_text.setColor(Color(255, 255, 255));
}

void PlayerEventBanner::draw()
{
    if (!m_visible)
        return;

    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();

    m_background.draw();
    m_text.draw();

    Drawable::popMatrix();
}

void PlayerEventBanner::update(const PlayerEvents &events)
{
    constexpr float paddingFactor = 0.1f;

    if (m_visible && events.getPerfectBlocks().empty())
        m_visible = false;
    else if (!m_visible && !events.getPerfectBlocks().empty())
    {
        m_text.setText(_(BannerPerfectBlock));

        const float padding = m_text.getWidth() * paddingFactor;
        const float bannerWidth = m_text.getWidth() + padding;
        const float bannerHeight = m_text.getHeight() + padding;

        m_text.setPosition(padding / 2.f,
            bannerHeight * .72f - m_text.getOriginHeight());

        m_background = Polygon::rectangle(
            bannerWidth, bannerHeight,
            Color(0, 0, 0, 140));
        m_background.setBorderSize(0.f);

        //Center the banner origin
        setOrigin(bannerWidth / 2.f, bannerHeight / 2.f);

        m_visible = true;
    }
}
