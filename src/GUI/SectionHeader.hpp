/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SECTION_HEADER
#define DEF_SECTION_HEADER

#include "GUI/Widget/TextButton.hpp"
#include "Graphics/Drawable/Polygon.hpp"
#include <string>

class TextureManager;
class FontManager;
class SoundEngine;
struct Vector;

/// Screen/section header with a title and a subtitle
class SectionHeader
{
    public:
        SectionHeader(const std::string &title, const std::string &subtitle,
                      TextureManager &textureManager, FontManager &fontManager,
                      SoundEngine &soundEngine, const Vector &viewSize);
        void draw();

        /// Mouse released event on the back button
        bool backRelease(const Vector &mouseCoords);
        /// Mouse moved event on the back button
        bool backMove(const Vector &mouseCoords);

        float getHeight() const;

    private:
        const float m_viewWidth;

        Text m_title, m_subtitle;
        TextButton m_backButton;
        Polygon m_background;
};

#endif
