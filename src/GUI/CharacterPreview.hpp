/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CHARACTER_PREVIEW
#define DEF_CHARACTER_PREVIEW

#include "Animation/Animation.hpp"
#include "Graphics/Drawable/AnimationDrawer.hpp"
#include <pugixml.hpp>

class TextureManager;
struct Box;

/// Preview of a selected character: Useful for character selection
class CharacterPreview : public Drawable
{
    public:
        CharacterPreview();
        void draw() override;

        void load(const std::string &characterName, TextureManager &manager);
        void update(float frameTime);

        float getWidth() const;
        float getHeight() const;
        Box getBodyBox() const;

        void select();
        void unselect();

    private:
        void loadAnimation(const pugi::xml_node &animNode, Animation &dst) const;
        void loadAnimations(const std::string &player, TextureManager &manager);

        Animation m_defaultAnim, m_selectedAnim;
        AnimationDrawer m_defaultDrawer, m_selectedDrawer;
        bool m_selected;
};

#endif
