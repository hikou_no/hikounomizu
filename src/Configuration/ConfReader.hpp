/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CONF_READER
#define DEF_CONF_READER

#ifndef CONF_DISABLE_PLAYERKEYS
    class PlayerKeys;
#endif

#ifndef CONF_DISABLE_LOGS
    #include "Tools/Log.hpp"
    #include "Tools/Format.hpp"
#endif

#include <map>
#include <string>
#include <sstream>
#include <cstdint>

/// Class to read and access key/value configuration items
class ConfReader
{
    public:
        /// Loads a set of configuration items from a file
        bool load(const std::string &filePath);

        /// Gets the std::string value associated with a given key, if found
        bool getItem(const std::string &elementKey, std::string &target) const;

        /// Gets the value associated with a given key as a std::uint8_t,
        /// i.e., as an integer between 0 and 255, not as a character
        bool getItem(const std::string &elementKey, std::uint8_t &target) const
        {
            std::string elementValue;
            if (!getItem(elementKey, elementValue))
                return false;

            unsigned int requestedItem = 0;
            if (!convertItem(elementValue, requestedItem) ||
                requestedItem > 255)
            {
                #ifndef CONF_DISABLE_LOGS
                    Log::err(Format::format("Could not read configuration "
                                        "item {} as type uint8", elementKey));
                #endif
                return false;
            }

            target = static_cast<std::uint8_t>(requestedItem);
            return true;
        }

        /// Gets the value associated with a given key and
        /// attempts to convert it to type T
        template <typename T>
        bool getItem(const std::string &elementKey, T &target) const
        {
            std::string elementValue;
            if (!getItem(elementKey, elementValue))
                return false;
            else if (!convertItem(elementValue, target))
            {
                #ifndef CONF_DISABLE_LOGS
                    Log::err(Format::format("Could not read configuration "
                                        "item {} as type {}",
                                        elementKey, typeid(T).name()));
                #endif
                return false;
            }

            return true;
        }

        #ifndef CONF_DISABLE_PLAYERKEYS
            /// Utility method to retrieve the player keys contained in the
            /// current configuration items
            void getPlayerKeysList(PlayerKeys &target) const;
        #endif

    private:
        /// Attempts to convert a std::string value to an arbitrary type T
        template <typename T>
        bool convertItem(const std::string &elementValue, T &target) const
        {
            T typedItem;

            std::istringstream convertToType(elementValue);
            convertToType >> typedItem;

            if (!convertToType.fail())
            {
                target = typedItem;
                return true;
            }

            return false;
        }

        std::map<std::string, std::string> m_confItems;
};

#endif
