/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_COLOR
#define DEF_COLOR

#include <cstdint>

struct Color
{
    Color() : Color(0, 0, 0) {}

    Color(std::uint8_t r, std::uint8_t g, std::uint8_t b, std::uint8_t a = 255):
    red(r), green(g), blue(b), alpha(a)
    {

    }

    std::uint8_t red, green, blue, alpha;
};

#endif
