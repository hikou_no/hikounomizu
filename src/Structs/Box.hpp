/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_BOX
#define DEF_BOX

#include "Vector.hpp"

struct Box
{
    Box() : left(0.f), top(0.f), width(0.f), height(0.f) {}

    Box(float x, float y, float w, float h) :
    left(x), top(y), width(w), height(h)
    {

    }

    void fadeInto(const Box &target, float fadeRatio)
    {
        const float leftOffset = (target.left - left) * fadeRatio;
        const float topOffset = (target.top - top) * fadeRatio;
        const float rightOffset =
            ((target.left+target.width) - (left+width)) * fadeRatio;
        const float bottomOffset =
            ((target.top+target.height) - (top+height)) * fadeRatio;

        left += leftOffset;
        top += topOffset;
        width += rightOffset - leftOffset;
        height += bottomOffset - topOffset;
    }

    bool contains(const Vector &point) const
    {
        return left < point.x && left + width > point.x &&
               top < point.y && top + height > point.y;
    }

    bool contains(const Box &box, float epsilon) const
    {
        return left - epsilon < box.left &&
               left + width + epsilon > box.left + box.width &&
               top - epsilon < box.top &&
               top + height + epsilon > box.top + box.height;
    }

    bool hits(const Box &box, float epsilon) const
    {
        return left + width - epsilon > box.left &&
               left + epsilon < box.left + box.width &&
               top + height - epsilon > box.top &&
               top + epsilon < box.top + box.height;
    }

    Vector getCenter() const
    {
        return Vector(left + width / 2.f, top + height / 2.f);
    }

    float left, top, width, height;
};

#endif
