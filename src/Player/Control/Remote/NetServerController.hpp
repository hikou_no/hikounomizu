/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_NET_SERVER_CONTROLLER
#define DEF_NET_SERVER_CONTROLLER

#include "Player/Control/PlayerController.hpp"
#include "Network/Serialization/PlayerStatus.hpp"

class PlayerAction;

/// Takes remote player actions as input
/// and can return the current player status of the associated player
class ServerPlayerInterface
{
    public:
        virtual ~ServerPlayerInterface() {}

        virtual void playerActionReceived(const PlayerAction &action) = 0;
        virtual const PlayerStatus &getPlayerStatus() = 0;

        virtual void reset() = 0;
};

class NetServerController : public PlayerController,
                            public ServerPlayerInterface
{
    public:
        NetServerController();
        void act() override;

        //ServerPlayerInterface callback
        void playerActionReceived(const PlayerAction &action) override;
        const PlayerStatus &getPlayerStatus() override;

        /// Reset the known moving/crouching state of the player
        void reset() override;

    private:
        PlayerStatus m_playerStatus;

        bool m_blocking, m_crouching, m_movingLeft, m_movingRight;
};

#endif
