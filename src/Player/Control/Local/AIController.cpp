/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "AIController.hpp"

#include <algorithm>
#include <cassert>

namespace
{
    /// Returns whether an enemy is KO, useful to remove KO enemies
    bool isKO(const Player *enemy)
    {
        return (enemy == nullptr || enemy->isKo());
    }

    /// Returns whether \p player is currently facing \p target
    bool looksTowards(const Player &player, const Player &target)
    {
        const float playerCenter = player.getBox().getCenter().x;
        const float targetCenter = target.getBox().getCenter().x;
        return (playerCenter < targetCenter && !player.looksLeft()) ||
            (playerCenter > targetCenter && player.looksLeft());
    }

    /// Returns whether \p player is currently over or under \p target
    bool verticallyAligned(const Player &player, const Player &target)
    {
        const Box &playerBox = player.getBox();
        const Box &targetBox = target.getBox();

        return (playerBox.left > targetBox.left &&
                playerBox.left < targetBox.left + targetBox.width) ||
            (targetBox.left > playerBox.left &&
             targetBox.left < playerBox.left + playerBox.width);
    }

    AIPersonality getPersonalityForPlayer(const std::string &name)
    {
        if (name == "Hana")
            return AIPersonality{270.f, 800.f, 1200.f, 100.f, 30,
                Timer::duration_float_ms(4000.f)};
        else if (name == "Hikou")
            return AIPersonality{220.f, 350.f, 600.f, 100.f, 10,
                Timer::duration_float_ms(2000.f)};
        else if (name == "Takino")
            return AIPersonality{210.f, 450.f, 900.f, 100.f, 20,
                Timer::duration_float_ms(3000.f)};

        return AIPersonality{};
    }

    ///
    /// Behaviour that stops the AI player as long as no enemy is known
    ///
    class BehaviourIdle : public AIBehaviour
    {
        public:
            AIBehaviour::Mode act(Player &self, const Player *enemy) override
            {
                if (self.isMoving())
                    self.setMoving(MovingState::NotMoving);

                if (self.isCrouching())
                    self.uncrouch();

                if (self.isBlocking())
                    self.unblock();

                return (enemy == nullptr) ?
                    AIBehaviour::Mode::Idle :
                    AIBehaviour::Mode::GetClose;
            }
    };

    ///
    /// Behaviour that attempts to get the AI player
    /// to a close range of the enemy
    ///
    class BehaviourGetClose : public AIBehaviour
    {
        public:
            AIBehaviour::Mode act(Player &self, const Player *enemy) override
            {
                if (enemy == nullptr)
                    return AIBehaviour::Mode::Idle;

                const float distance = self.getBox().getCenter().x -
                    enemy->getBox().getCenter().x;
                const float absDistance = fabsf(distance);

                if (absDistance < m_personality.closeRangeDistance)
                {
                    BehaviourIdle{}.act(self, enemy); //Reset moving state
                    return AIBehaviour::Mode::CloseRange;
                }
                else if (absDistance < m_personality.midRangeDistance)
                {
                    //Getting close: maybe attack
                    const int action = m_dist(m_generator);
                    if (action <= 2)
                        self.punch();
                    else if (action <= 4)
                        self.kick();
                    else if (action == 5)
                        self.jump();
                }

                if (distance > 0.0) self.setMoving(MovingState::MovingLeft);
                else self.setMoving(MovingState::MovingRight);

                constexpr float stuckEpsilon = 10.0;
                if (fabsf(m_lastPosition.x - self.getBox().left) < stuckEpsilon)
                    self.jump();

                m_lastPosition = Vector(self.getBox().left, self.getBox().top);
                return AIBehaviour::Mode::GetClose;
            }

        private:
            Vector m_lastPosition;
            std::mt19937 m_generator{std::random_device{}()};
            std::uniform_int_distribution<int> m_dist{1, 30};
    };

    ///
    /// Behaviour that attempts to get the AI player
    /// to a long range of the enemy
    ///
    class BehaviourGetAway : public AIBehaviour
    {
        public:
            AIBehaviour::Mode act(Player &self, const Player *enemy) override
            {
                if (enemy == nullptr)
                    return AIBehaviour::Mode::Idle;

                const float distance = self.getBox().getCenter().x -
                    enemy->getBox().getCenter().x;

                if (fabsf(distance) > m_personality.longRangeDistance)
                {
                    BehaviourIdle{}.act(self, enemy); //Reset moving state
                    return AIBehaviour::Mode::LongRange;
                }

                if (distance > 0.0) self.setMoving(MovingState::MovingRight);
                else self.setMoving(MovingState::MovingLeft);

                constexpr float stuckEpsilon = 10.0;
                if (fabsf(m_lastPosition.x - self.getBox().left) < stuckEpsilon)
                {
                    if (self.isOnGround()) //Maybe stuck because of terrain
                        self.jump();
                    else //Stuck beyond jumping away
                    {
                        BehaviourIdle{}.act(self, enemy); //Reset moving state
                        return AIBehaviour::Mode::LongRange;
                    }
                }

                m_lastPosition = Vector(self.getBox().left, self.getBox().top);
                return AIBehaviour::Mode::GetAway;
            }

        private:
            Vector m_lastPosition;
    };

    ///
    /// Behaviour that performs close range fighting
    ///
    class BehaviourCloseRange : public AIBehaviour
    {
        public:
            AIBehaviour::Mode act(Player &self, const Player *enemy) override
            {
                if (enemy == nullptr)
                    return AIBehaviour::Mode::Idle;

                //Always face the enemy
                if (!looksTowards(self, (*enemy)))
                    self.setLooksLeft(!self.looksLeft());

                const float distance = self.getBox().getCenter().x -
                    enemy->getBox().getCenter().x;

                if (fabsf(distance) > m_personality.closeRangeDistance)
                {
                    BehaviourIdle{}.act(self, enemy); //Reset moving state
                    return AIBehaviour::Mode::GetClose;
                }
                else if (verticallyAligned(self, (*enemy)))
                {
                    BehaviourIdle{}.act(self, enemy); //Reset moving state
                    return AIBehaviour::Mode::Unreachable;
                }
                else if (enemy->isAttacking() && looksTowards((*enemy), self))
                    return AIBehaviour::Mode::AttackIncoming;
                else
                {
                    const int offensiveAction = m_dist(m_generator);
                    if (offensiveAction == 0) //Toggle crouching
                    {
                        if (self.isCrouching()) self.uncrouch();
                        else self.crouch();
                    }
                    else if (offensiveAction == 1) //Punch
                        self.punch();
                    else if (offensiveAction == 2) //Kick
                        self.kick();
                    else if (offensiveAction == 3) //Toggle blocking
                    {
                        if (self.isBlocking()) self.unblock();
                        else self.block();
                    }
                    else if (offensiveAction == 4) //Jump
                        self.jump();
                }

                return AIBehaviour::Mode::CloseRange;
            }

        private:
            std::mt19937 m_generator{std::random_device{}()};
            std::uniform_int_distribution<int> m_dist{0, 4};
    };

    ///
    /// Behaviour that handles an incoming close range attack
    ///
    class BehaviourAttackIncoming : public AIBehaviour
    {
        public:
            AIBehaviour::Mode act(Player &self, const Player *enemy) override
            {
                if (enemy == nullptr)
                    return AIBehaviour::Mode::Idle;

                if (!enemy->isAttacking() || !looksTowards((*enemy), self))
                {
                    self.uncrouch();
                    self.unblock();
                    if (m_dist(m_generator) <= m_personality.getAwayLikelihood)
                        return AIBehaviour::Mode::GetAway;
                    else
                        return AIBehaviour::Mode::CloseRange;
                }

                if (enemy->isCrouching()) self.crouch();
                else self.uncrouch();
                self.block();

                return AIBehaviour::Mode::AttackIncoming;
            }

        private:
            std::mt19937 m_generator{std::random_device{}()};
            std::uniform_int_distribution<std::size_t> m_dist{1, 100};
    };

    ///
    /// Behaviour that handles an incoming close range attack
    ///
    class BehaviourLongRange : public AIBehaviour
    {
        public:
            AIBehaviour::Mode act(Player &self, const Player *enemy) override
            {
                if (enemy == nullptr)
                    return AIBehaviour::Mode::Idle;

                //Always face the enemy
                if (!looksTowards(self, (*enemy)))
                    self.setLooksLeft(!self.looksLeft());

                const float distance = fabsf(self.getBox().getCenter().x -
                    enemy->getBox().getCenter().x);

                if (distance < m_personality.closeRangeDistance ||
                    (distance < m_personality.midRangeDistance &&
                     enemy->isAttacking() &&
                     looksTowards((*enemy), self)))
                {
                    BehaviourIdle{}.act(self, enemy); //Reset moving state
                    return AIBehaviour::Mode::CloseRange;
                }

                const int action = m_dist(m_generator);
                if (action == 1 &&
                    fabsf(enemy->getBox().top - self.getBox().top) <
                        m_personality.verticalThrowThreshold) //Throw projectile
                {
                    self.throwShuriken();
                }
                else if (action == 2) //Jump
                    self.jump();
                else if (action == 3) //Toggle block
                {
                    if (self.isBlocking()) self.unblock();
                    else self.block();
                }

                //Handle inactivity
                constexpr float inactiveEpsilon = 10.0;
                if (fabsf(m_lastPosition.x - self.getBox().left) > inactiveEpsilon)
                    m_inactive.reset();
                else if (m_inactive.getTicks() >
                    m_personality.longRangeMaxDuration)
                {
                    BehaviourIdle{}.act(self, enemy); //Reset moving state
                    return AIBehaviour::Mode::GetClose;
                }

                m_lastPosition = Vector(self.getBox().left, self.getBox().top);
                return AIBehaviour::Mode::LongRange;
            }

        private:
            Vector m_lastPosition;
            Timer m_inactive;

            std::mt19937 m_generator{std::random_device{}()};
            std::uniform_int_distribution<int> m_dist{1, 3};
    };

    ///
    /// Behaviour that handles the AI being stuck over or under the enemy
    ///
    class BehaviourUnreachable : public AIBehaviour
    {
        public:
            AIBehaviour::Mode act(Player &self, const Player *enemy) override
            {
                if (enemy == nullptr)
                    return AIBehaviour::Mode::Idle;

                const float distance = fabsf(self.getBox().getCenter().x -
                    enemy->getBox().getCenter().x);

                if (distance > m_personality.closeRangeDistance)
                {
                    BehaviourIdle{}.act(self, enemy); //Reset moving state
                    return AIBehaviour::Mode::CloseRange;
                }

                if (enemy->looksLeft())
                    self.setMoving(MovingState::MovingRight);
                else
                    self.setMoving(MovingState::MovingLeft);

                constexpr float stuckEpsilon = 10.0;
                if (fabsf(m_lastPosition.x - self.getBox().left) < stuckEpsilon)
                {
                    //Closer than close range but not unreachable anymore
                    if (!verticallyAligned(self, (*enemy)))
                    {
                        BehaviourIdle{}.act(self, enemy); //Reset moving state
                        return AIBehaviour::Mode::CloseRange;
                    }
                    else //Attempt alternative actions
                    {
                        const int action = m_dist(m_generator);
                        if (action == 0) //Change direction?
                            self.setLooksLeft(!self.looksLeft());
                        else if (action < 5)
                            self.punch(); //Punch the obstacle away?
                        else
                        {
                            //Crouch-push it?
                            if (self.isCrouching())
                                self.uncrouch();
                            else
                                self.crouch();
                        }
                    }
                }

                m_lastPosition = Vector(self.getBox().left, self.getBox().top);
                return AIBehaviour::Mode::Unreachable;
            }

        private:
            Vector m_lastPosition;
            std::mt19937 m_generator{std::random_device{}()};
            std::uniform_int_distribution<int> m_dist{1, 10};
    };
}

AIController::AIController() : PlayerController(), PlayerCallbackReceiver(),
m_generator(std::random_device{}()), m_targetEnemy(nullptr)
{
    m_behaviours[static_cast<std::size_t>(AIBehaviour::Mode::Idle)] =
        std::make_unique<BehaviourIdle>();
    m_behaviours[static_cast<std::size_t>(AIBehaviour::Mode::GetClose)] =
        std::make_unique<BehaviourGetClose>();
    m_behaviours[static_cast<std::size_t>(AIBehaviour::Mode::GetAway)] =
        std::make_unique<BehaviourGetAway>();
    m_behaviours[static_cast<std::size_t>(AIBehaviour::Mode::CloseRange)] =
        std::make_unique<BehaviourCloseRange>();
    m_behaviours[static_cast<std::size_t>(AIBehaviour::Mode::AttackIncoming)] =
        std::make_unique<BehaviourAttackIncoming>();
    m_behaviours[static_cast<std::size_t>(AIBehaviour::Mode::LongRange)] =
        std::make_unique<BehaviourLongRange>();
    m_behaviours[static_cast<std::size_t>(AIBehaviour::Mode::Unreachable)] =
        std::make_unique<BehaviourUnreachable>();
}

void AIController::attach(Player &player)
{
    PlayerController::attach(player);
    player.addCallbackReceiver((*this));

    for (std::unique_ptr<AIBehaviour> &behaviour : m_behaviours)
    {
        assert(behaviour != nullptr);
        behaviour->setPersonality(getPersonalityForPlayer(player.getName()));
    }
}

void AIController::act()
{
    if (m_player == nullptr ||
        m_player->getAttack() != AttackIndex::Default ||
        m_player->isKo())
        return;

    constexpr Timer::duration_float_ms maxReactionTime(100.f);
    if (m_clock.getTicks() > maxReactionTime)
    {
        //Remove KO enemies (not to focus on a KO player)
        removeKoEnemies();

        assert(m_behaviours.size() > static_cast<std::size_t>(m_mode));
        assert(m_behaviours[static_cast<std::size_t>(m_mode)] != nullptr);

        //Act and potentially update behaviour
        m_mode = m_behaviours[static_cast<std::size_t>(m_mode)]->act(
            (*m_player), m_targetEnemy);

        m_clock.reset();
    }
}

void AIController::playerWasHit(float /*strength*/, HitDirection /*side*/,
                                const Player *source)
{
    //The AI was hit by a player, focus them
    if (source != nullptr)
        m_targetEnemy = source;
}

//Ignoring this callback here, only focus on impact from known other players
void AIController::playerHitTriggered(bool /*blocked*/) {}

void AIController::setEnemies(const std::vector<const Player*> &enemies)
{
    m_enemies.clear();
    for (const Player *enemy : enemies)
    {
        if (enemy != nullptr && enemy != m_player)
            m_enemies.push_back(enemy);
    }

    updateTargetEnemy();
}

void AIController::updateTargetEnemy()
{
    if (m_enemies.empty())
        m_targetEnemy = nullptr;
    else
    {
        std::uniform_int_distribution<std::size_t> enemyDist(0,
            m_enemies.size() - 1);
        m_targetEnemy = m_enemies[enemyDist(m_generator)];
    }
}

void AIController::removeKoEnemies()
{
    std::vector<const Player*>::const_iterator it_removed =
        std::remove_if(m_enemies.begin(), m_enemies.end(), isKO);

    if (it_removed != m_enemies.end())
    {
        m_enemies.erase(it_removed, m_enemies.end());
        updateTargetEnemy();
    }
}
