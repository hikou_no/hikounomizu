/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_AI_CONTROLLER
#define DEF_AI_CONTROLLER

#include "Player/Control/PlayerController.hpp"
#include "Player/Player.hpp" ///< PlayerCallbackReceiver
#include "Tools/Timer.hpp"
#include <random>
#include <array>
#include <vector>
#include <memory>
#include <cstddef>

/// Settings to customize the behaviour of the AI for each character
struct AIPersonality
{
    /// Distance at which close range combat is engaged
    float closeRangeDistance = 300.f;

    /// Transition zone between close and long range combat
    float midRangeDistance = 500.f;

    /// Distance at which long range combat is engaged
    float longRangeDistance = 800.f;

    /// Difference in y-position at which a projectile may be thrown
    float verticalThrowThreshold = 100.f;

    /// Out of a 100, likelihood that the AI will move away to long range combat
    /// after the opponent attacked close range
    ///(as opposed to staying close range)
    std::size_t getAwayLikelihood = 10;

    /// Maximum duration spent not moving in a long range position
    Timer::duration_float_ms longRangeMaxDuration{2000.f};
};

/// State of AI behaviour, performing an action and reporting state changes
class AIBehaviour
{
    public:
        enum class Mode : std::size_t
        {
            Idle = 0, ///< Not doing anything
            GetClose = 1, ///< Moving towards the selected enemy
            GetAway = 2, ///< Moving away from the selected enemy
            CloseRange = 3, ///< Fighting close range
            AttackIncoming = 4, ///< Close range attack incoming
            LongRange = 5, ///< Fighting close range
            Unreachable = 6 ///< The enemy is unreachable (vertically aligned)
        };
        enum : std::size_t { ModeCount = 7 };

        virtual ~AIBehaviour() = default;

        /// Act and return the (optionally updated) mode
        virtual Mode act(Player &self, const Player *enemy) = 0;

        void setPersonality(const AIPersonality &personality)
        {
            m_personality = personality;
        }

    protected:
        AIPersonality m_personality;
};

class AIController : public PlayerController, public PlayerCallbackReceiver
{
    public:
        AIController();

        void attach(Player &player) override;

        void act() override;
        void playerWasHit(float /*strength*/, HitDirection /*side*/,
                          const Player *source) override;
        void playerHitTriggered(bool blocked) override;

        void setEnemies(const std::vector<const Player*> &enemies);

    private:
        void removeKoEnemies(); ///< Remove all KO enemies from m_enemies
        void updateTargetEnemy(); ///< Randomly choose a target enemy

        std::mt19937 m_generator;

        std::vector<const Player*> m_enemies; ///< All enemies
        const Player *m_targetEnemy; ///< The enemy the AI is focusing on

        AIBehaviour::Mode m_mode = AIBehaviour::Mode::Idle;
        std::array<std::unique_ptr<AIBehaviour>,
                   AIBehaviour::ModeCount> m_behaviours{};

        Timer m_clock; ///< Useful to delay reaction times
};

#endif
