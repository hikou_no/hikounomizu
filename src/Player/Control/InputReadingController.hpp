/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_INPUT_READING_CONTROLLER
#define DEF_INPUT_READING_CONTROLLER

#include "PlayerController.hpp"
#include "Player/PlayerKeys.hpp"
#include "Tools/Input/InputMonitor.hpp"

class InputReadingController : public PlayerController
{
    public:
        /// Construct with optional initial input state
        explicit InputReadingController(bool moveLeftDown = false,
                                        bool moveRightDown = false,
                                        bool crouchedDown = false,
                                        bool blockDown = false);

        /// To be called before updateKeyState()
        void keyEvent(const SDL_Event &event);
        void updateKeyState(const SDL_Event &event);

        void setDeviceID(int id);
        void setKeys(const PlayerKeys &keys);

    protected:
        /// Instant action keys
        virtual void punchPressed() = 0;
        virtual void kickPressed() = 0;
        virtual void throwPressed() = 0;
        virtual void jumpPressed() = 0;

        /// Status changing key
        bool isMoveLeftDown() const;
        bool isMoveRightDown() const;
        bool isCrouchDown() const;
        bool isBlockDown() const;

    private:
        InputMonitor m_inputMonitor;
        PlayerKeys m_keys;
        Uint8 m_deviceID;

        bool m_moveLeftDown, m_moveRightDown, m_crouchedDown, m_blockDown;
};

#endif
