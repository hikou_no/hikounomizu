/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYER_MOVES
#define DEF_PLAYER_MOVES

#include "StateDescription.hpp"
#include "Animation/CallbackAnimation.hpp"
#include "Attack.hpp"
#include "Tools/Timer.hpp"
#include <pugixml.hpp>
#include <map>
#include <string>

struct State
{
    CallbackAnimation animation;
    std::map<AttackIndex, Attack> attacks;
};

class PlayerMoves
{
    public:
        PlayerMoves();

        void reset();
        void update(float frameTime);

        bool mayAttack();

        void setAttack(AttackIndex attack, bool force = false);
        void setState(StateIndex state, bool force = false);

        /// Update the state, attack and animation time
        void force(StateIndex state, AttackIndex attack, float animTime);

        AttackIndex getAttack() const;
        StateIndex getState() const;
        const Animation *getAnimation() const;
        std::map<StateIndex, const State*> getStateInfo() const;

        /// Whether the player has started blocking a short time ago,
        /// and is not spamming the block action
        bool isPerfectlyBlocking() const;

        float getWidth() const;
        float getHeight() const;

        void load(const std::string &xmlRelPath,
                  const std::string &animationsPath, Player &player);

    private:
        /// Forces the time of the current animation to \p time
        /// Useful to apply networked server information to a client player
        void setAnimationTime(float time);

        void loadAnimation(const std::string &path,
                           const pugi::xml_node &animNode,
                           CallbackAnimation &dst) const;

        std::map<StateIndex, State> m_statesList;

        StateIndex m_state; ///< Current state
        AttackIndex m_attack; ///< Current attack

        StateIndex m_targetState; ///< Target of a transition

        /// Time value recorded at the start of the last started block
        Timer::TimePoint m_lastBlockStartTime;

        /// Time value recorded at the end of the last ended block
        Timer::TimePoint m_lastBlockEndTime;
};

#endif
