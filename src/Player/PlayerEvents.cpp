/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PlayerEvents.hpp"

void PlayerEvents::update(float frameTime)
{
    const Timer::duration_float_ms duration(frameTime);

    std::list<PerfectBlockingEvent>::iterator it = m_perfectBlocks.begin();
    while (it != m_perfectBlocks.end())
    {
        it->lifetime -= duration;

        if (it->lifetime.count() <= 0.f)
            it = m_perfectBlocks.erase(it);
        else
            ++it;
    }
}

void PlayerEvents::reset()
{
    m_perfectBlocks.clear();
}

void PlayerEvents::addPerfectBlock(const Box &damageBox)
{
    m_perfectBlocks.push_back({m_nextId++, damageBox});
}

void PlayerEvents::addPerfectBlock(std::uint8_t id, const Box &damageBox)
{
    for (const PerfectBlockingEvent &block : m_perfectBlocks)
    {
        if (block.id == id)
            return; //Perfect block already known: skip
    }

    m_perfectBlocks.push_back({id, damageBox});
}

bool PlayerEvents::consumePerfectBlock(PerfectBlockingEvent &dst) const
{
    if (m_perfectBlocks.empty())
        return false;

    for (PerfectBlockingEvent &block : m_perfectBlocks)
    {
        if (!block.consumed)
        {
            dst = block;
            block.consumed = true;
            return true;
        }
    }

    return false;
}

const std::list<PerfectBlockingEvent> &PlayerEvents::getPerfectBlocks() const
{
    return m_perfectBlocks;
}
