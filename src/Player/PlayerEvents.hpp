/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYER_EVENTS
#define DEF_PLAYER_EVENTS

#include "Tools/Timer.hpp"
#include "Structs/Box.hpp"
#include <list>
#include <cstdint>

struct PerfectBlockingEvent
{
    std::uint8_t id = 0;
    Box damageBox;

    /// Whether the event was consumed (particle field fired)
    bool consumed = false;

    /// Time until the event is discarded
    Timer::duration_float_ms lifetime = Timer::duration_float_ms(
        #ifdef HNM_SERVER
            200.f  //Allow enough lifetime for the server to send out
                   //multiple packets notifying of the unique events
        #else
            1000.f //Allow a longer lifetime on the client-side to avoid
                   //late packets retriggering previously-known events
        #endif
    );
};

/// Keeps track of player gameplay-related events (perfect blocking).
/// Useful to keep track of unique events that trigger external effects
/// (e.g. particle field) and allow their communication in networked games
class PlayerEvents
{
    public:
        /// Handle discarding of events based on their lifetime
        void update(float frameTime);

        /// Immediately discard all the events. Maintains the next id
        void reset();

        /// Add a perfect block event
        void addPerfectBlock(const Box &damageBox);

        /// Add a perfect block event of \p id if not already known
        /// Maintains the next id
        void addPerfectBlock(std::uint8_t id, const Box &damageBox);

        /// Retrieve the last unprocessed perfect blocking event
        bool consumePerfectBlock(PerfectBlockingEvent &dst) const;

        /// Retrieve all currently known perfect blocking events
        const std::list<PerfectBlockingEvent> &getPerfectBlocks() const;

    private:
        std::uint8_t m_nextId = 0;
        mutable std::list<PerfectBlockingEvent> m_perfectBlocks;
};

#endif
