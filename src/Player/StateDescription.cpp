/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StateDescription.hpp"

namespace StateDescription
{
    std::uint8_t writeStateIndex(StateIndex stateIx)
    {
        return static_cast<std::uint8_t>(stateIx);
    }

    StateIndex readStateIndex(std::uint8_t stateIx)
    {
        if (stateIx > IndexLimit::State)
            return StateIndex::None;

        return static_cast<StateIndex>(stateIx);
    }

    std::uint8_t writeAttackIndex(AttackIndex attackIx)
    {
        return static_cast<std::uint8_t>(attackIx);
    }

    AttackIndex readAttackIndex(std::uint8_t attackIx)
    {
        if (attackIx > IndexLimit::Attack)
            return AttackIndex::Default;

        return static_cast<AttackIndex>(attackIx);
    }

    std::uint8_t writeMovingState(MovingState movingState)
    {
        return static_cast<std::uint8_t>(movingState);
    }

    MovingState readMovingState(std::uint8_t movingState)
    {
        if (movingState > IndexLimit::Moving)
            return MovingState::NotMoving;

        return static_cast<MovingState>(movingState);
    }

    bool isBlocking(StateIndex stateIx)
    {
        return stateIx == StateIndex::StandingBlock ||
            stateIx == StateIndex::CrouchedBlock;
    }

    bool isCrouching(StateIndex stateIx)
    {
        return stateIx == StateIndex::Crouched ||
            stateIx == StateIndex::CrouchedBlock;
    }
}
