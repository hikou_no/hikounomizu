/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_BIT_TOOLS
#define DEF_BIT_TOOLS

#include <cstdint>
#include <vector>

/// Describes the layout for packing several integers into an uint8_t
/// e.g.: BitPack::_71 packs a 7-bit and a 1-bit integer
///       BitPack::_431 packs a 4-bit, a 3-bit, and a 1-bit integer
enum class BitPack {_71, _44, _431, _21};

namespace BitTools
{
    std::uint8_t packUInt8(BitPack alignment, const std::vector<std::uint8_t> &values);
    std::vector<std::uint8_t> unpackUInt8(BitPack alignment, const std::uint8_t &packed);
}

#endif
