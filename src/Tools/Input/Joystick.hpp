/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_JOYSTICK
#define DEF_JOYSTICK

#include <SDL2/SDL.h>
#include <string>

class Key;

class Joystick
{
    public:
        Joystick();
        Joystick(const Joystick &copied) = delete;
        Joystick &operator=(const Joystick &copied) = delete;
        ~Joystick();

        bool load(int joyIndex);

        std::string getName() const;
        int getInstanceID() const;
        Key getKeyFor(SDL_GameControllerButton button) const;

    private:
        void close();

        SDL_GameController *m_gameController;
        SDL_Joystick *m_joystick;
};

#endif
