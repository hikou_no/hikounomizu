/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_WEAPON_INVENTORY
#define DEF_WEAPON_INVENTORY

#include "WeaponType.hpp"
#include <map>

class WeaponEther;
class Player;
class PhysicsWorld;
class InventoryStatus;

/// Represents the weapon inventory of a player
/// Keeps track of the remaining weapons, throws only if weapons are available
class WeaponInventory
{
    public:
        WeaponInventory();
        WeaponInventory(const Player &player, WeaponEther &ether);

        void throwShuriken(PhysicsWorld &world);
        unsigned int getRemaining(WeaponType weaponType) const;

        void reset(); ///< Reset the default amount of weapons in the inventory
        void applyStatus(const InventoryStatus &status);

    private:
        const Player *m_player;
        WeaponEther *m_ether; ///< The weapon ether to throw available weapons

        /// By weapon type, how many weapons available
        std::map<WeaponType, unsigned int> m_slots;
};

#endif
