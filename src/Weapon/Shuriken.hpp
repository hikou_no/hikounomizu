/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SHURIKEN
#define DEF_SHURIKEN

#define SHURIKEN_SIZE 30.f

#include "Network/Serialization/WeaponStatus.hpp"
#include "Tools/Timer.hpp"
#include "Weapon.hpp"
#include <string>

/// A rotating shuriken with parabolic trajectory
class Shuriken : public Weapon
{
    public:
        Shuriken(WeaponEther &ether, unsigned int id);
        void setPhysicsWorld(PhysicsWorld &physicsWorld) override;

        void update(float frameTime) override;

        void applyStatus(const WeaponStatus &status) override;
        const WeaponStatus &getStatus() const override;

        float getAngle() const override;
        const std::string &getTexturePath() const override;
        const Box &getSourceBox() const override;

        void takeAHit(float /*strength*/, HitDirection /*side*/,
                      const Box &/*damageBox*/, bool /*blockable*/,
                      const Player */*source*/) override;

    private:
        const std::string m_texturePath;
        const Box m_shurikenBox;
        const float m_rotationRate; ///< Number of rotations per second

        const float m_sfxPeriod;
        Timer m_sfxTimer;

        float m_currentAngle;
        mutable WeaponStatus m_status;
};

#endif
