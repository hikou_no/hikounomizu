/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Shuriken.hpp"

#include "Engines/Sound/SoundInterface.hpp"
#include "Engines/Sound/SoundEffectsList.hpp"

Shuriken::Shuriken(WeaponEther &ether, unsigned int id) : Weapon(ether, id),
m_texturePath("gfx/weapons/shuriken.png"),
m_shurikenBox(Box(0.f, 0.f, 128.f, 128.f)), m_rotationRate(3.f),
m_sfxPeriod(120.f), m_currentAngle(0.f)
{
    PhysicsObject::setAirPenetration(.9f);
}

void Shuriken::setPhysicsWorld(PhysicsWorld &physicsWorld)
{
    PhysicsObject::setPhysicsWorld(physicsWorld);

    //Init physics box
    setWidth(SHURIKEN_SIZE);
    setHeight(SHURIKEN_SIZE);
}

void Shuriken::update(float frameTime)
{
    float rotationDiff = 360.f * m_rotationRate * frameTime/1000.f;
    m_currentAngle += (getVelocity().x >= 0.f) ? rotationDiff : -rotationDiff;

    //Sound effect timer handling
    if (m_soundInterface != nullptr && m_sfxTimer.getElapsed() > m_sfxPeriod)
    {
        m_soundInterface->playSoundEffect("airfriction",
                                          CHARACTER_AGNOSTIC_EFFECT);
        m_sfxTimer.reset();
    }
}

void Shuriken::applyStatus(const WeaponStatus &status)
{
    if (status.getType() == WeaponType::Shuriken &&
        status.getIdentifier() == m_inEtherID)
    {
        status.applyToWeapon((*this));
    }
}

const WeaponStatus &Shuriken::getStatus() const
{
    m_status.readFromWeapon(WeaponType::Shuriken, m_inEtherID, (*this));
    return m_status;
}

float Shuriken::getAngle() const
{
    return m_currentAngle;
}

const std::string &Shuriken::getTexturePath() const
{
    return m_texturePath;
}

const Box &Shuriken::getSourceBox() const
{
    return m_shurikenBox;
}

void Shuriken::takeAHit(float /*strength*/, HitDirection /*side*/,
                        const Box &/*damageBox*/, bool /*blockable*/,
                        const Player */*source*/)
{
    if (m_soundInterface != nullptr)
        m_soundInterface->playSoundEffect("impact_shuriken",
                                          CHARACTER_AGNOSTIC_EFFECT, .4f);
    queueForRemoval();
}
