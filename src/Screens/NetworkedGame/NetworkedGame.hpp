/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_NETWORKED_GAME
#define DEF_NETWORKED_GAME

#include "Screens/Screen.hpp"

class GameEngine;
class Window;
class Configuration;
class Client;

/// Screen inheriting class holding a network game, including the network lobby,
/// server connection and network fight scene phases
class NetworkedGame : public Screen
{
    public:
        explicit NetworkedGame(GameEngine &gameEngine);

        //Screen virtual method
        void run() override;

    private:
        bool initialize(Client &client) const;
        void deinitialize() const;

        GameEngine *m_gameEngine;

        Window *m_window;
        Configuration *m_configuration;
};

#endif
