/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_MAIN_MENU
#define DEF_MAIN_MENU

#include "Screens/Screen.hpp"
#include "MainMenuTab.hpp"

class GameEngine;
class Window;
class Configuration;
class FightRules;

/// Screen inheriting class representing the main menu screen
class MainMenu : public Screen
{
    public:
        explicit MainMenu(GameEngine &gameEngine);

        //Screen virtual method
        void run() override;

    private:
        void processResult(MainMenuTab::Result result,
                           const FightRules &fightRules);

        GameEngine *m_gameEngine;

        Window *m_window;
        Configuration *m_configuration;
};

#endif
