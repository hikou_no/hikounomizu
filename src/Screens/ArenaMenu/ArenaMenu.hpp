/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ARENA_MENU
#define DEF_ARENA_MENU

#include "Arena/Arena.hpp"
#include "Graphics/Drawable/ArenaDrawer.hpp"
#include "Graphics/Drawable/Polygon.hpp"

#include "Engines/Physics/PhysicsWorld.hpp"
#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Resources/SoundBufferManager.hpp"
#include "Engines/Resources/FontManager.hpp"

#include "Structs/Box.hpp"
#include "ArenaPreviewCallback.hpp"
#include "Screens/Screen.hpp"

#include <vector>
#include <string>

class GameEngine;
class Window;
class Configuration;

class ArenaMenu : public Screen, public ArenaPreviewCallback
{
    public:
        explicit ArenaMenu(GameEngine &gameEngine);
        void previewChanged(std::size_t arenaIx) override;

        //Screen virtual method
        void run() override;

    private:
        void fetchNames(const std::string &xmlRelPath);

        TextureManager m_textureManager;
        SoundBufferManager m_soundBufferManager;
        FontManager m_fontManager;

        /// Arena name / Arena path name
        std::vector< std::pair<std::string, std::string> > m_arenas;
        std::size_t m_currentArena; ///< Index of the currently drawn arena

        Arena m_previewArena; ///< Preview Arena of m_currentArena
        ArenaDrawer m_previewDrawer; ///< Drawer for m_previewArena
        Polygon m_previewBackground; ///< Background for m_previewDrawer

        /// PhysicsWorld for m_previewArena platforms
        PhysicsWorld m_previewWorld;

        /// Source arena box and target arena display box
        Box m_previewBox, m_previewTargetBox;

        GameEngine *m_gameEngine;
        Window *m_window;
        Configuration *m_configuration;
};

#endif
