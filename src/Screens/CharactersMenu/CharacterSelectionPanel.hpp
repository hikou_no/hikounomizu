/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CHARACTER_SELECTION_PANEL
#define DEF_CHARACTER_SELECTION_PANEL

#include "GUI/Widget/Tab.hpp"
#include "GUI/CharacterPreview.hpp"
#include "GUI/ShadowText.hpp"
#include "Graphics/Drawable/Polygon.hpp"
#include "Structs/Character.hpp"
#include <cstddef>
#include <vector>

/// Horizontal Panel of characters preview: Useful for character selection
class CharacterSelectionPanel : public Tab
{
    public:
        CharacterSelectionPanel();
        CharacterSelectionPanel(Font &textFont, std::size_t charactersNo,
                                float viewWidth);
        void draw() override;

        void update(float frameTime);

        void setCharacter(std::size_t ix,
                          const Character &character,
                          TextureManager &textureManager);
        void resetCharacter(std::size_t ix);
        Character getCharacter(std::size_t ix) const;

        bool readyCharacter(std::size_t ix);
        void unreadyCharacter(std::size_t ix);

        std::size_t getSize() const;

        bool mouseRelease(const Vector &mouseCoords) override;
        bool mouseClick(const Vector &mouseCoords) override;
        bool mouseMove(const Vector &mouseCoords) override;

    private:
        void updatePosition(std::size_t ix);
        void updatePreviewOrigin(CharacterPreview &preview);

        struct CharacterData
        {
            Character character;
            ShadowText name;
            Polygon background;
            CharacterPreview preview;
        };
        std::vector<CharacterData> m_characters;

        /// Stores the list of known valid characters
        std::vector<std::string> m_knownCharacters;
        float m_viewWidth;
};

#endif
