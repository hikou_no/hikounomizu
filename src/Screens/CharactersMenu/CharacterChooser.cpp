/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CharacterChooser.hpp"
#include "CharacterSelectionCallback.hpp"

#include "Structs/Character.hpp"

CharacterChooser::CharacterChooser(Font &textFont, const SkinBox &skin,
                                   const SkinSound &sounds, float spacing) :
Tab(skin.box.width * 3.f + spacing * 2.f,
    skin.box.height * 2.f + spacing,
    Color(0, 0, 0, 0)),
m_hikou(Character("Hikou", Character::Type::Human).getFullName(), skin, sounds),
m_hikouAI(Character("Hikou", Character::Type::AI).getFullName(), skin, sounds),
m_takino(Character("Takino", Character::Type::Human).getFullName(), skin, sounds),
m_takinoAI(Character("Takino", Character::Type::AI).getFullName(), skin, sounds),
m_hana(Character("Hana", Character::Type::Human).getFullName(), skin, sounds),
m_hanaAI(Character("Hana", Character::Type::AI).getFullName(), skin, sounds),
m_callback(nullptr)
{
    m_hikou.setTextFont(textFont);
    m_hikouAI.setTextFont(textFont);

    m_takino.setTextFont(textFont);
    m_takinoAI.setTextFont(textFont);

    m_hana.setTextFont(textFont);
    m_hanaAI.setTextFont(textFont);

    m_hikou.setTextColor(Color(255, 255, 255));
    m_hikouAI.setTextColor(Color(255, 255, 255));

    m_takino.setTextColor(Color(255, 255, 255));
    m_takinoAI.setTextColor(Color(255, 255, 255));

    m_hana.setTextColor(Color(255, 255, 255));
    m_hanaAI.setTextColor(Color(255, 255, 255));

    m_hikouAI.setYPosition(skin.box.height + spacing);

    m_takino.setXPosition(skin.box.width + spacing);
    m_takinoAI.setPosition(skin.box.width + spacing,
                           skin.box.height + spacing);

    m_hana.setXPosition(2.f * (skin.box.width + spacing));
    m_hanaAI.setPosition(2.f * (skin.box.width + spacing),
                         skin.box.height + spacing);
}

void CharacterChooser::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();

    //Draw
    m_background.draw();

    m_hikou.draw();
    m_hikouAI.draw();

    m_takino.draw();
    m_takinoAI.draw();

    m_hana.draw();
    m_hanaAI.draw();

    Drawable::popMatrix();
}

void CharacterChooser::initialize(CharacterSelectionCallback &callback)
{
    m_callback = &callback;
}


//Widget methods
bool CharacterChooser::mouseRelease(const Vector &mouseCoords)
{
    if (m_callback == nullptr)
        return false;

    Vector localMouse = Widget::localCoords(mouseCoords);

    if (m_hikou.mouseRelease(localMouse) || m_hikouAI.mouseRelease(localMouse) ||
        m_takino.mouseRelease(localMouse) || m_takinoAI.mouseRelease(localMouse) ||
        m_hana.mouseRelease(localMouse) || m_hanaAI.mouseRelease(localMouse))
    {
        m_callback->characterSelected();
    }

    return Widget::contains(localMouse);
}

bool CharacterChooser::mouseClick(const Vector &mouseCoords)
{
    if (m_callback == nullptr)
        return false;

    Vector localMouse = Widget::localCoords(mouseCoords);
    return Widget::contains(localMouse);
}

bool CharacterChooser::mouseMove(const Vector &mouseCoords)
{
    if (m_callback == nullptr)
        return false;

    Vector localMouse = Widget::localCoords(mouseCoords);

    if (m_hikou.mouseMove(localMouse))
        m_callback->characterFocused(Character("Hikou", Character::Type::Human));

    if (m_hikouAI.mouseMove(localMouse))
        m_callback->characterFocused(Character("Hikou", Character::Type::AI));

    if (m_takino.mouseMove(localMouse))
        m_callback->characterFocused(Character("Takino", Character::Type::Human));

    if (m_takinoAI.mouseMove(localMouse))
        m_callback->characterFocused(Character("Takino", Character::Type::AI));

    if (m_hana.mouseMove(localMouse))
        m_callback->characterFocused(Character("Hana", Character::Type::Human));

    if (m_hanaAI.mouseMove(localMouse))
        m_callback->characterFocused(Character("Hana", Character::Type::AI));

    return Widget::contains(localMouse);
}
