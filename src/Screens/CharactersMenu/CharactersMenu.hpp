/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CHARACTERS_MENU
#define DEF_CHARACTERS_MENU

#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Resources/SoundBufferManager.hpp"
#include "Engines/Resources/FontManager.hpp"

#include "CharacterSelectionCallback.hpp"
#include "CharacterSelectionPanel.hpp"

#include "Screens/Screen.hpp"

class GameEngine;
class Window;
class Configuration;

/// Screen inheriting class representing the characters selection menu
class CharactersMenu : public Screen, public CharacterSelectionCallback
{
    public:
        explicit CharactersMenu(GameEngine &gameEngine);
        void setPlayersNo(std::size_t playersNo);

        //CharacterSelectionCallback virtual methods
        void characterSelected() override;
        void characterFocused(const Character &character) override;

        //Screen virtual method
        void run() override;

    private:
        void cleanPreview();

        TextureManager m_textureManager;
        SoundBufferManager m_soundBufferManager;
        FontManager m_fontManager;

        CharacterSelectionPanel m_selectionPreview;
        std::size_t m_selectionIx;

        GameEngine *m_gameEngine;
        Window *m_window;
        Configuration *m_configuration;

        std::size_t m_playersNo;
};

#endif
