/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PauseTab.hpp"
#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Sound/SoundEngine.hpp"

#include "Tools/ScaledPixel.hpp"
#include "Tools/I18n.hpp" ///< _ macro for i18n

PauseTab::PauseTab(Font &headerFont, Font &textFont,
                   TextureManager &manager, SoundEngine &soundEngine,
                   float viewWidth) :
Tab(ScaledPixel{viewWidth}(260.f), ScaledPixel{viewWidth}(290.f)),
m_pauseText(3.f), m_choice(PauseTab::Result::NoChoice), m_active(false)
{
    const ScaledPixel sp(viewWidth);

    const Sprite ui_default(manager.getTexture("gfx/ui/ui.png"),
        Box(266.f / 512.f, 0.f, 246.f / 512.f, 71.f / 512.f));
    const Sprite ui_hover(manager.getTexture("gfx/ui/ui.png"),
        Box(266.f / 512.f, 75.f / 512.f, 246.f / 512.f, 71.f / 512.f));

    SkinBox skin(sp(240.f), sp(60.f), ui_default, ui_hover);
    SkinSound sounds(soundEngine, "audio/ui/menuFocus.ogg",
                     "audio/ui/menuClick.ogg");

    m_pauseText.setText(_(LocalGamePauseTitle));
    m_pauseText.setFont(headerFont);
    m_pauseText.setColor(Color(255, 255, 255), Color(50, 93, 151));
    m_pauseText.setPosition((getWidth() - m_pauseText.getWidth()) / 2.f,
                            sp(10.f));

    //Resume button
    m_resume.setText(_(PauseResume));
    m_resume.setTextFont(textFont);
    m_resume.setTextColor(Color(255, 255, 255));

    m_resume.setSkin(skin);
    m_resume.setSoundSkin(sounds);

    //Restart button
    m_restart.setText(_(LocalGamePauseRestart));
    m_restart.setTextFont(textFont);
    m_restart.setTextColor(Color(255, 255, 255));

    m_restart.setSkin(skin);
    m_restart.setSoundSkin(sounds);

    //Exit button
    m_exit.setText(_(PauseToMainMenu));
    m_exit.setTextFont(textFont);
    m_exit.setTextColor(Color(255, 255, 255));

    m_exit.setSkin(skin);
    m_exit.setSoundSkin(sounds);

    m_resume.setPosition((getWidth() - m_resume.getWidth()) / 2.f, sp(90.f));

    m_restart.setPosition((getWidth() - m_restart.getWidth()) / 2.f,
        m_resume.getYPosition() + m_resume.getHeight() + sp(10.f));

    m_exit.setPosition((getWidth() - m_exit.getWidth()) / 2.f,
        m_restart.getYPosition() + m_restart.getHeight() + sp(10.f));
}

void PauseTab::draw()
{
    if (m_active)
    {
        //Update matrix
        Drawable::pushMatrix();
        Drawable::updateMatrix();

        m_background.draw();

        m_pauseText.draw();
        m_resume.draw();
        m_restart.draw();
        m_exit.draw();

        Drawable::popMatrix();
    }
}

bool PauseTab::mouseRelease(const Vector &mouseCoords)
{
    if (!m_active)
        return false;

    Vector localMouse = Widget::localCoords(mouseCoords);

    if (!chosen())
    {
        if (m_resume.mouseRelease(localMouse))
            m_choice = PauseTab::Result::Resume;
        else if (m_restart.mouseRelease(localMouse))
            m_choice = PauseTab::Result::Restart;
        else if (m_exit.mouseRelease(localMouse))
            m_choice = PauseTab::Result::Exit;
    }

    return Widget::contains(localMouse);
}


bool PauseTab::mouseClick(const Vector &mouseCoords)
{
    if (!m_active)
        return false;

    Vector localMouse = Widget::localCoords(mouseCoords);
    return Widget::contains(localMouse);
}

bool PauseTab::mouseMove(const Vector &mouseCoords)
{
    if (!m_active)
        return false;

    Vector localMouse = Widget::localCoords(mouseCoords);

    m_resume.mouseMove(localMouse);
    m_restart.mouseMove(localMouse);
    m_exit.mouseMove(localMouse);

    return Widget::contains(localMouse);
}

void PauseTab::setActive(bool active)
{
    m_active = active;
    if (!m_active)
    {
        m_choice = PauseTab::Result::NoChoice;
        m_resume.setFocus(false);
        m_restart.setFocus(false);
        m_exit.setFocus(false);
    }
}

bool PauseTab::active() const
{
    return m_active;
}

bool PauseTab::chosen() const
{
    return (m_choice != PauseTab::Result::NoChoice);
}

PauseTab::Result PauseTab::result() const
{
    return m_choice;
}
