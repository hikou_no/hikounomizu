/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "EndTab.hpp"
#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Sound/SoundEngine.hpp"

#include "Tools/ScaledPixel.hpp"
#include "Tools/I18n.hpp" ///< _ macro for i18n

EndTab::EndTab(Font &textFont, TextureManager &manager,
               SoundEngine &soundEngine, float viewWidth) :
Tab(ScaledPixel{viewWidth}(520.f), ScaledPixel{viewWidth}(80.f)),
m_choice(EndTab::Result::NoChoice), m_active(false)
{
    const Sprite ui_default(manager.getTexture("gfx/ui/ui.png"),
        Box(266.f / 512.f, 0.f, 246.f / 512.f, 71.f / 512.f));
    const Sprite ui_hover(manager.getTexture("gfx/ui/ui.png"),
        Box(266.f / 512.f, 75.f / 512.f, 246.f / 512.f, 71.f / 512.f));

    SkinBox skin(ScaledPixel{viewWidth}(240.f),
                 ScaledPixel{viewWidth}(60.f), ui_default, ui_hover);
    SkinSound sounds(soundEngine,
                     "audio/ui/menuFocus.ogg",
                     "audio/ui/menuClick.ogg");

    m_replay.setText(_(LocalGameEndReplay));
    m_replay.setTextFont(textFont);
    m_replay.setTextColor(Color(255.f, 255.f, 255.f));

    m_replay.setSkin(skin);
    m_replay.setSoundSkin(sounds);

    m_exit.setText(_(PauseToMainMenu));
    m_exit.setTextFont(textFont);
    m_exit.setTextColor(Color(255.f, 255.f, 255.f));

    m_exit.setSkin(skin);
    m_exit.setSoundSkin(sounds);

    const float space = (getWidth() - skin.box.width * 2.f) / 3.f;
    m_replay.setPosition(space, (getHeight() - skin.box.height) / 2.f);
    m_exit.setPosition(m_replay.getWidth() + 2.f * space,
                       m_replay.getYPosition());
}

void EndTab::draw()
{
    if (m_active)
    {
        //Update matrix
        Drawable::pushMatrix();
        Drawable::updateMatrix();

        m_background.draw();

        m_replay.draw();
        m_exit.draw();

        Drawable::popMatrix();
    }
}

bool EndTab::mouseRelease(const Vector &mouseCoords)
{
    if (!m_active)
        return false;

    Vector localMouse = Widget::localCoords(mouseCoords);

    if (!chosen())
    {
        if (m_replay.mouseRelease(localMouse))
            m_choice = EndTab::Result::Replay;
        else if (m_exit.mouseRelease(localMouse))
            m_choice = EndTab::Result::Exit;
    }

    return Widget::contains(localMouse);
}

bool EndTab::mouseClick(const Vector &mouseCoords)
{
    if (!m_active)
        return false;

    Vector localMouse = Widget::localCoords(mouseCoords);
    return Widget::contains(localMouse);
}

bool EndTab::mouseMove(const Vector &mouseCoords)
{
    if (!m_active)
        return false;

    Vector localMouse = Widget::localCoords(mouseCoords);

    m_replay.mouseMove(localMouse);
    m_exit.mouseMove(localMouse);

    return Widget::contains(localMouse);
}

void EndTab::setActive(bool active)
{
    m_active = active;
    if (!m_active)
    {
        m_choice = EndTab::Result::NoChoice;
        m_replay.setFocus(false);
        m_exit.setFocus(false);
    }
}

bool EndTab::active() const
{
    return m_active;
}

bool EndTab::chosen() const
{
    return (m_choice != EndTab::Result::NoChoice);
}

EndTab::Result EndTab::result() const
{
    return m_choice;
}
