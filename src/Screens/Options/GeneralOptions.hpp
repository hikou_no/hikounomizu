/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_GENERAL_OPTIONS
#define DEF_GENERAL_OPTIONS

#include "Graphics/Display.hpp"
#include "GUI/Widget/Tab.hpp"
#include "GUI/Widget/Checkbox.hpp"
#include "GUI/Widget/Chooser.hpp"
#include "GUI/Widget/ContinuousChooser.hpp"
#include "GUI/Widget/TextButton.hpp"
#include "GUI/LanguageFlag.hpp"
#include "Tools/I18n.hpp"
#include <vector>

class Configuration;
class FontManager;
class Font;
class TextureManager;
class SoundEngine;

struct LabelParams
{
    LabelParams(Text &&labelText, float labelWidth, float xPos, float yPos) :
    text(labelText), width(labelWidth), xPosition(xPos), yPosition(yPos) {}

    Text text;
    float width, xPosition, yPosition;
};

class GeneralOptions : public Tab
{
    public:
        GeneralOptions(Configuration &configuration,
                       FontManager &fontManager,
                       TextureManager &textureManager,
                       SoundEngine &soundEngine,
                       const Display::DisplayMode &activeMode,
                       float viewWidth);
        void draw() override;

        bool mouseRelease(const Vector &mouseCoords) override;
        bool mouseClick(const Vector &mouseCoords) override;
        bool mouseMove(const Vector &mouseCoords) override;

        /// Sets the list of supported fullscreen video mode sizes
        void setDisplaySizes(const std::vector<std::pair<int, int>> &sizes);

        /// Whether the human user wishes to apply display mode changes
        bool displayModeApplied() const;

         /// Whether the vertical sync or frame rate parameters were changed
        bool refreshRateUpdated() const;

        /// Whether the master, music or effects volume was updated
        bool volumeUpdated() const;

        /// Whether the locale was updated
        bool localeUpdated() const;

    private:
        void apply(bool display = false, bool defaultDisplay = false);
        void positionWidget(LabelParams &&label, Widget &widget);

        Text m_headerVideo, m_headerRefresh, m_headerAudio,
             m_headerLocale, m_headerMisc;
        std::vector<Text> m_labels;

        Checkbox m_dynamicCamera, m_cameraShake;
        Checkbox m_showFps, m_showHitboxes, m_vsync;
        Chooser m_displayMode, m_resolution, m_locale;
        ContinuousChooser m_masterVolume;
        ContinuousChooser m_musicVolume, m_ambientVolume, m_effectsVolume;
        ContinuousChooser m_framerate;
        TextButton m_applyDisplayMode, m_restoreDefaultDisplay;

        /// Text describing that the display mode will be applied on restart
        /// Shown when changing from/to a real fullscreen mode
        /// A restart is required then to avoid glitches
        Text m_fullscreenOnRestart;

        mutable bool m_displayModeApplied;
        mutable bool m_refreshRateUpdated;
        mutable bool m_volumeUpdated;
        mutable bool m_localeUpdated;

        Configuration *m_configuration;

        const std::vector<I18n::Locale> m_localeList;
        LanguageFlag m_languageFlags;
};

#endif
