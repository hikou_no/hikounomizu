/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PARTICLE_FIELD
#define DEF_PARTICLE_FIELD

#include "Particle.hpp"
#include <pugixml.hpp>
#include <memory>
#include <random>
#include <vector>
#include <cstddef>

class ParticleField
{
    public:
        virtual ~ParticleField() = default;

        virtual std::vector<PolygonParticle> emit(
            const Vector &position) const = 0;

        static std::unique_ptr<ParticleField> alloc(const pugi::xml_node &node);
};

/// A particle field that shoots particles radially away from a central point
class RadialParticleField : public ParticleField
{
    public:
        RadialParticleField(const Polygon &shape, std::size_t count,
            Timer::duration_float_ms lifetime, float speed, float damping);

        std::vector<PolygonParticle> emit(
            const Vector &position) const override;

    private:
        Polygon m_shape;

        std::size_t m_particleCount;
        Timer::duration_float_ms m_particleLifetime;
        float m_particleSpeed, m_particleDamping;

        mutable std::mt19937 m_generator;
};

/// A particle field that merges particle fields to be emitted simultaneously
class GroupParticleField : public ParticleField
{
    public:
        GroupParticleField() = default;
        void addField(std::unique_ptr<ParticleField> field);
        bool empty() const;

        std::vector<PolygonParticle> emit(
            const Vector &position) const override;

    private:
        std::vector<std::unique_ptr<ParticleField>> m_fields;
};

#endif
