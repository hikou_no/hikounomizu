/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PlayerParticles.hpp"

#include "Tools/BuildValues.hpp" ///< Generated at build time

PlayerParticles::PlayerParticles() :
m_defaultField(
    Polygon::circle(10.f, 16, Color(0, 100, 200)), 5,
    Timer::duration_float_ms(500.f), 200.f, .4f)
{

}

void PlayerParticles::load(const std::string &charactersXmlPath)
{
    //Locate absolute path to xml data
    std::string xmlPath = BuildValues::data(charactersXmlPath);

    pugi::xml_document xmlFile;
    if (!xmlFile.load_file(xmlPath.c_str()))
        return;

    pugi::xml_node characterNode;
    for (characterNode = xmlFile.child("main").child("character");
         characterNode;
         characterNode = characterNode.next_sibling("character"))
    {
        const std::string characterName(
            characterNode.attribute("name").value());

        const pugi::xml_node perfectBlockNode =
            characterNode.child("particles")
                         .child("perfectBlock").child("field");
        if (!perfectBlockNode)
            continue;

        std::unique_ptr<ParticleField> perfectBlockField =
            ParticleField::alloc(perfectBlockNode);

        if (perfectBlockField != nullptr)
            m_particles.emplace(characterName, std::move(perfectBlockField));
    }
}

const ParticleField &PlayerParticles::getPerfectBlock(
    const std::string &characterName) const
{
    std::map<std::string, std::unique_ptr<ParticleField>>::const_iterator it =
        m_particles.find(characterName);

    if (it != m_particles.end())
        return (*(it->second.get()));

    return m_defaultField;
}
