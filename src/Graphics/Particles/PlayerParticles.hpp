/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYER_PARTICLES
#define DEF_PLAYER_PARTICLES

#include "ParticleField.hpp"
#include <map>
#include <memory>
#include <string>

/// Loader for player particle fields
class PlayerParticles
{
    public:
        PlayerParticles();
        void load(const std::string &charactersXmlPath);
        const ParticleField &getPerfectBlock(
            const std::string &characterName) const;

    private:
        std::map<std::string, std::unique_ptr<ParticleField>> m_particles;
        RadialParticleField m_defaultField;
};

#endif
