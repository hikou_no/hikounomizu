/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Particle.hpp"

#include "Graphics/GL.hpp"
#include <cstddef>

namespace
{
    void addVertices(const std::vector<float> &vertices,
                     const Vector &offset, std::vector<float> &dst)
    {
        for (std::size_t i = 0; i < vertices.size(); i++)
        {
            if (i % 2 == 0)
                dst.push_back(vertices[i] + offset.x);
            else
                dst.push_back(vertices[i] + offset.y);
        }
    }

    void addColor(const Color &color, float alpha, std::size_t vertexCount,
                  std::vector<GLubyte> &dst)
    {
        std::vector<GLubyte> colors =
        {
            color.red, color.green, color.blue,
            static_cast<GLubyte>(color.alpha * alpha)
        };

        for (std::size_t i = 0; i < vertexCount; i++)
            std::copy(colors.begin(), colors.end(), std::back_inserter(dst));
    }
}

void ParticleTools::draw(const std::vector<PolygonParticle> &particles)
{
    //Arbitrary initial reserved amount of memory per particle
    constexpr std::size_t reserved = 16; //Assuming 16 vertices per particle

    std::vector<float> trianglesArray;
    trianglesArray.reserve(particles.size() * reserved * 2); //x,y coordinates

    std::vector<float> linesArray;
    linesArray.reserve(particles.size() * reserved * 2); //x,y coordinates

    std::vector<GLubyte> colorArray;
    colorArray.reserve(particles.size() * reserved * 4); //rgba color

    std::vector<GLubyte> borderColorArray;
    borderColorArray.reserve(particles.size() * reserved * 4); //rgba color

    for (const PolygonParticle &particle : particles)
    {
        const std::vector<float> &trigs = particle.polygon.getTrianglesArray();
        const std::vector<float> &lines = particle.polygon.getLinesArray();
        const Vector particlePosition =
            particle.data.position - particle.polygon.getCentroid();

        addVertices(trigs, particlePosition, trianglesArray);
        addVertices(lines, particlePosition, linesArray);

        addColor(particle.polygon.getColor(), particle.data.alpha,
                 trigs.size() / 2, colorArray);
        addColor(particle.polygon.getBorderColor(), particle.data.alpha,
                 lines.size() / 2, borderColorArray);
    }

    #ifndef _WIN32
        glEnable(GL_MULTISAMPLE);
    #endif
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);

    //Draw backgrounds
    glVertexPointer(2, GL_FLOAT, 0,
        static_cast<const GLvoid*>(trianglesArray.data()));
    glColorPointer(4, GL_UNSIGNED_BYTE, 0,
        static_cast<const GLvoid*>(colorArray.data()));
    glDrawArrays(GL_TRIANGLES, 0,
        static_cast<GLsizei>(trianglesArray.size() / 2));

    //Draw borders
    glVertexPointer(2, GL_FLOAT, 0,
        static_cast<const GLvoid*>(linesArray.data()));
    glColorPointer(4, GL_UNSIGNED_BYTE, 0,
            static_cast<const GLvoid*>(borderColorArray.data()));
    glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(linesArray.size() / 2));

    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);

    //Disable loaded modules
    glDisable(GL_BLEND);

    #ifndef _WIN32
        glDisable(GL_MULTISAMPLE);
    #endif
}
