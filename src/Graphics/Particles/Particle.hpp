/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PARTICLE
#define DEF_PARTICLE

#include "Graphics/Drawable/Polygon.hpp"
#include "Structs/Vector.hpp"
#include "Tools/Timer.hpp"

struct ParticleData
{
    Vector position;
    Vector velocity;
    float alpha;

    float damping; ///< Fraction of velocity kept per second
    Timer::duration_float_ms lifetime;
};

struct PolygonParticle
{
    Polygon polygon;
    ParticleData data;
};

namespace ParticleTools
{
    void draw(const std::vector<PolygonParticle> &particles);
}

#endif
