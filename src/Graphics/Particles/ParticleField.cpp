/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ParticleField.hpp"

namespace
{
    Color readColor(const pugi::xml_node &colorNode)
    {
        const std::size_t red = colorNode.attribute("r").as_uint(0);
        const std::size_t green = colorNode.attribute("g").as_uint(0);
        const std::size_t blue = colorNode.attribute("b").as_uint(0);
        const std::size_t alpha = colorNode.attribute("a").as_uint(255);

        return Color(
            red   < 256 ? static_cast<std::uint8_t>(red)   : 0,
            green < 256 ? static_cast<std::uint8_t>(green) : 0,
            blue  < 256 ? static_cast<std::uint8_t>(blue)  : 0,
            alpha < 256 ? static_cast<std::uint8_t>(alpha) : 0
        );
    }

    std::unique_ptr<RadialParticleField> loadRadialField(
        const pugi::xml_node &node)
    {
        const std::size_t particleCount = node.attribute("count").as_uint(0);
        const Timer::duration_float_ms lifetime(
            node.attribute("lifetime").as_float(1.f) * 1000.f);
        const float speed = node.attribute("speed").as_float(0.f);
        const float damping = node.attribute("damping").as_float(1.f);

        const pugi::xml_node particleNode = node.child("particle");
        if (!particleNode)
            return nullptr;

        const pugi::xml_node backgroundNode =
            particleNode.child("backgroundColor");
        const pugi::xml_node borderNode = particleNode.child("borderColor");
        const pugi::xml_node shapeNode = particleNode.child("shape");
        if (!shapeNode ||
            std::string(shapeNode.attribute("type").value()) != "circle")
            return nullptr;

        const float radius = shapeNode.attribute("radius").as_float(0.f);
        const unsigned int points = shapeNode.attribute("points").as_uint(16);
        Polygon particle = Polygon::circle(
            radius > 0.f ? radius : 0.f,
            points, readColor(backgroundNode), readColor(borderNode));

        return std::make_unique<RadialParticleField>(
            particle, particleCount, lifetime, speed, damping);
    }

    std::unique_ptr<GroupParticleField> loadGroupField(
        const pugi::xml_node &node)
    {
        std::unique_ptr<GroupParticleField> groupField =
            std::make_unique<GroupParticleField>();

        pugi::xml_node fieldNode;
        for (fieldNode = node.child("field");
             fieldNode;
             fieldNode = fieldNode.next_sibling("field"))
        {
            groupField->addField(ParticleField::alloc(fieldNode));
        }

        if (groupField->empty())
            return nullptr;

        return groupField;
    }
}

/////////////////////////
///RadialParticleField///
/////////////////////////
RadialParticleField::RadialParticleField(
    const Polygon &shape, std::size_t count,
    Timer::duration_float_ms lifetime, float speed, float damping) :
m_shape(shape), m_particleCount(count), m_particleLifetime(lifetime),
m_particleSpeed(speed), m_particleDamping(damping),
m_generator(std::random_device{}())
{

}

std::vector<PolygonParticle> RadialParticleField::emit(
    const Vector &position) const
{
    constexpr float PI = 3.14159265f;
    static std::uniform_real_distribution<float> angleDist(0.f, 2.f * PI);

    std::vector<PolygonParticle> particles;

    for (std::size_t i = 0; i < m_particleCount; i++)
    {
        const float angle = angleDist(m_generator);
        particles.push_back(
        {
            m_shape,
            {
                position,
                Vector(static_cast<float>(cos(angle)),
                       static_cast<float>(sin(angle))) * m_particleSpeed,
                1.f, m_particleDamping, m_particleLifetime
            }
        });
    }

    return particles;
}

std::unique_ptr<ParticleField> ParticleField::alloc(const pugi::xml_node &node)
{
    const std::string type(node.attribute("type").value());
    if (type == "radial")
        return loadRadialField(node);
    else if (type == "group")
        return loadGroupField(node);

    return nullptr;
}

////////////////////////
///GroupParticleField///
////////////////////////
void GroupParticleField::addField(std::unique_ptr<ParticleField> field)
{
    if (field != nullptr)
        m_fields.push_back(std::move(field));
}

std::vector<PolygonParticle> GroupParticleField::emit(
    const Vector &position) const
{
    std::vector<PolygonParticle> emitted;
    for (const std::unique_ptr<ParticleField> &field : m_fields)
    {
        const std::vector<PolygonParticle> particles = field->emit(position);
        emitted.insert(emitted.end(), particles.begin(), particles.end());
    }

    return emitted;
}

bool GroupParticleField::empty() const
{
    return m_fields.empty();
}
