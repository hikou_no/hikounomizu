/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ParticleEther.hpp"
#include "ParticleField.hpp"

#include <cmath>

namespace
{
    void updatePhysics(ParticleData &particleData,
                       std::chrono::duration<float> time)
    {
        particleData.velocity = particleData.velocity *
            static_cast<float>(pow(particleData.damping, time.count()));
        particleData.position = particleData.position +
            particleData.velocity * time.count();
    }

    template <typename Particle>
    void updateParticles(std::vector<Particle> &particles, float frameTime)
    {
        typename std::vector<Particle>::iterator it = particles.begin();
        while (it != particles.end())
        {
            ParticleData &particle = it->data;
            particle.alpha -= frameTime / particle.lifetime.count();
            if (particle.alpha <= 0.f)
            {
                it = particles.erase(it);
                continue;
            }

            //Integrate velocity and position
            constexpr Timer::duration_float_ms physicsTicks(8.33f); //120 fps

            const std::size_t steps =
                static_cast<std::size_t>(frameTime / physicsTicks.count());
            const float remainingTime =
                static_cast<float>(fmod(frameTime, physicsTicks.count()));

            for (std::size_t i = 0; i < steps; i++)
                updatePhysics(particle, physicsTicks);

            updatePhysics(particle,
                std::chrono::duration<float>(remainingTime / 1000.f));

            ++it;
        }
    }
}

void ParticleEther::draw()
{
    ParticleTools::draw(m_polygonParticles);
}

void ParticleEther::update(float frameTime)
{
    updateParticles(m_polygonParticles, frameTime);
}

void ParticleEther::emit(const ParticleField &field, const Vector &position)
{
    const std::vector<PolygonParticle> emitted = field.emit(position);
    m_polygonParticles.insert(m_polygonParticles.end(),
        emitted.begin(), emitted.end());
}
