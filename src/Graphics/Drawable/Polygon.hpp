/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_POLYGON
#define DEF_POLYGON

#include "Drawable.hpp"
#include "Structs/Vector.hpp"
#include "Structs/Color.hpp"
#include <array>
#include <vector>

struct Triangle
{
    Triangle() = default;
    Triangle(Vector a, Vector b, Vector c) : points({a, b, c}) {}
    std::array<Vector, 3> points;
};

struct Line
{
    Line() = default;
    Line(Vector a, Vector b) : points({a, b}) {}
    std::array<Vector, 2> points;
};

class Polygon : public Drawable
{
    public:
        Polygon();
        void draw() override;

        void setUniformColor(const Color &color,
                             const Color &borderColor = Color());
        void setBorderSize(float borderSize);

        const Vector &getCentroid() const;
        float getBorderSize() const;
        const Color &getColor() const;
        const Color &getBorderColor() const;
        const std::vector<float> &getTrianglesArray() const;
        const std::vector<float> &getLinesArray() const;

        /// Generate a rectangle of given \p width and \p height
        static Polygon rectangle(float width, float height,
                                 const Color &color = Color(),
                                 const Color &borderColor = Color());

        /// Generate a circle of radius \p radius
        static Polygon circle(float radius, unsigned int precision = 360,
                              const Color &color = Color(),
                              const Color &borderColor = Color());

    private:
        void setTriangles(const std::vector<Triangle> &triangles);
        void setLines(const std::vector<Line> &lines);

        std::vector<float> m_triangles; ///< Triangle vertices
        std::vector<float> m_lines; ///< Line vertices

        Color m_color, m_borderColor;
        float m_borderSize;
        Vector m_centroid;
};

#endif
