/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ANIMATION_DRAWER
#define DEF_ANIMATION_DRAWER

#include "Graphics/GL.hpp"
#include "Drawable.hpp"
#include "Structs/Box.hpp"
#include <cstddef>

class Animation;
class Texture;

class AnimationDrawer : public Drawable
{
    public:
        AnimationDrawer();
        void draw() override;
        void attachTo(const Animation &animation, Texture &texture);

    private:
        void computeTextureCoordinates();

        const Animation *m_animation;
        Texture *m_texture;

        std::size_t m_drawFrameIx;
        Box m_drawBox;

        /// Texture coordinate parameters
        GLfloat m_x1, m_y1, m_x2, m_y2;
};

#endif
