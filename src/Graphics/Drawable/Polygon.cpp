/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Polygon.hpp"
#include "Graphics/GL.hpp"

#include <cmath>

namespace
{
    /** Compute the centroid of a polygon given its outline */
    Vector computeCentroid(const std::vector<Line> &lines)
    {
        if (lines.empty())
            return Vector();

        Vector centroid;
        for (const Line &line : lines)
            centroid = centroid + line.points[0];

        return centroid / static_cast<float>(lines.size());
    }
}

Polygon::Polygon() : Drawable(), m_borderSize(1.f) {}

void Polygon::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();

    //Enable needed modules
    #ifndef _WIN32
        glEnable(GL_MULTISAMPLE);
    #endif

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //Draw
    glEnableClientState(GL_VERTEX_ARRAY);

    //Background
    glColor4ub(m_color.red, m_color.green, m_color.blue, m_color.alpha);

    glVertexPointer(2, GL_FLOAT, 0,
        static_cast<const GLvoid*>(m_triangles.data()));
    glDrawArrays(GL_TRIANGLES, 0, static_cast<GLsizei>(m_triangles.size() / 2));

    //Border
    if (m_borderSize > 0.f)
    {
        glLineWidth(m_borderSize);
        glColor4ub(m_borderColor.red, m_borderColor.green,
                   m_borderColor.blue, m_borderColor.alpha);

        glVertexPointer(2, GL_FLOAT, 0,
            static_cast<const GLvoid*>(m_lines.data()));
        glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(m_lines.size() / 2));

        glLineWidth(1.f);
    }

    glColor4ub(255, 255, 255, 255); //Restore default color
    glDisableClientState(GL_VERTEX_ARRAY);

    //Disable loaded modules
    glDisable(GL_BLEND);

    #ifndef _WIN32
        glDisable(GL_MULTISAMPLE);
    #endif

    Drawable::popMatrix();
}

void Polygon::setUniformColor(const Color &color, const Color &borderColor)
{
    m_color = color;
    m_borderColor = borderColor;
}

void Polygon::setBorderSize(float borderSize)
{
    if (borderSize >= 0.f)
        m_borderSize = borderSize;
}

void Polygon::setTriangles(const std::vector<Triangle> &triangles)
{
    m_triangles.clear();
    m_triangles.reserve(triangles.size() * 6);
    for (const Triangle &triangle : triangles)
    {
        m_triangles.insert(m_triangles.end(),
        {
            triangle.points[0].x, triangle.points[0].y,
            triangle.points[1].x, triangle.points[1].y,
            triangle.points[2].x, triangle.points[2].y
        });
    }
}

void Polygon::setLines(const std::vector<Line> &lines)
{
    m_lines.clear();
    m_lines.reserve(lines.size() * 4);
    for (const Line &line : lines)
    {
        m_lines.insert(m_lines.end(),
        {
            line.points[0].x, line.points[0].y,
            line.points[1].x, line.points[1].y
        });
    }

    m_centroid = computeCentroid(lines);
}

const Vector &Polygon::getCentroid() const
{
    return m_centroid;
}

float Polygon::getBorderSize() const
{
    return m_borderSize;
}

const Color &Polygon::getColor() const
{
    return m_color;
}

const Color &Polygon::getBorderColor() const
{
    return m_borderColor;
}

const std::vector<float> &Polygon::getTrianglesArray() const
{
    return m_triangles;
}

const std::vector<float> &Polygon::getLinesArray() const
{
    return m_lines;
}

Polygon Polygon::rectangle(float width, float height,
                           const Color &color, const Color &borderColor)
{
    Polygon polygon;
    polygon.setUniformColor(color, borderColor);

    polygon.setTriangles({
        Triangle(Vector(0.f, 0.f), Vector(0.f, height), Vector(width, 0.f)),
        Triangle(Vector(width, 0.f), Vector(0.f, height), Vector(width, height))
    });

    polygon.setLines({
        Line(Vector(0.f, 0.f), Vector(0.f, height)),
        Line(Vector(0.f, height), Vector(width, height)),
        Line(Vector(width, height), Vector(width, 0.f)),
        Line(Vector(width, 0.f), Vector(0.f, 0.f))
    });

    return polygon;
}

Polygon Polygon::circle(float radius, unsigned int precision,
                        const Color &color, const Color &borderColor)
{
    constexpr float PI = 3.14159265f;

    std::vector<Triangle> triangles;
    triangles.reserve(precision);

    std::vector<Line> lines;
    lines.reserve(precision);

    const float step = 2.f * PI / static_cast<float>(precision);
    float lastAngle = 0.f;
    float angle = step;
    for (unsigned int i = 0; i < precision; i++)
    {
        const Vector lastAnglePoint(
            radius * static_cast<float>(cos(lastAngle)) + radius,
            radius * static_cast<float>(sin(lastAngle)) + radius);
        const Vector anglePoint(
            radius * static_cast<float>(cos(angle)) + radius,
            radius * static_cast<float>(sin(angle)) + radius);

        triangles.emplace_back(
            anglePoint, lastAnglePoint, Vector(radius, radius));
        lines.emplace_back(lastAnglePoint, anglePoint);

        lastAngle = angle;
        angle += step;
    }

    Polygon polygon;
    polygon.setUniformColor(color, borderColor);
    polygon.setTriangles(triangles);
    polygon.setLines(lines);
    return polygon;
}
