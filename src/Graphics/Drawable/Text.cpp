/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Text.hpp"
#include "Graphics/Resources/Font.hpp"
#include "Graphics/Resources/Texture.hpp"

#include "Tools/Utf8.hpp"

Text::Text() : Drawable(), m_font(nullptr),
m_originHeight(0.f), m_width(0.f), m_height(0.f)
{

}

Text::Text(const Utf8::utf8_string &text, Font &font) :
Drawable(), m_font(&font), m_text(text)
{
    loadGlyphs();
}

void Text::draw()
{
    if (m_font == nullptr)
        return;

    Texture &texture = m_font->getTexture();
    updateGLArrays();

    if (!m_glyphs.empty())
    {
        //Update matrix
        Drawable::pushMatrix();
        Drawable::updateMatrix();

        glColor4ub(m_color.red, m_color.green, m_color.blue, m_color.alpha);
        texture.draw(m_vertices, m_texCoords);
        glColor4ub(255, 255, 255, 255); //Restore default color

        Drawable::popMatrix();
    }
}

void Text::setFont(Font &font)
{
    m_font = &font;
    loadGlyphs();
}

const Font *Text::getFont() const
{
    return m_font;
}

void Text::setText(const Utf8::utf8_string &text)
{
    m_text = text;
    loadGlyphs();
}

const std::string &Text::getText() const
{
    return m_text.get();
}

void Text::setColor(const Color &color)
{
    m_color = color;
}

float Text::getWidth() const
{
    return m_width;
}

float Text::getHeight() const
{
    return m_height;
}

float Text::getOriginHeight() const
{
    return m_originHeight;
}

BoundingBox Text::getBoundingBox(std::size_t ix_begin, std::size_t ix_end) const
{
    if (ix_begin >= ix_end || ix_end > m_glyphs.size())
        return BoundingBox();

    BoundingBox boundingBox;
    for (std::size_t ix = 0; ix < ix_end; ix++)
    {
        const GlyphEntry &glyph = m_glyphs[ix].glyph;
        const Vector &bearing = glyph.topLeft;

        //Update left / width bounding box
        const float kerning = m_glyphs[ix].kerning;
        const float advance = glyph.advance.x;

        if (ix < ix_begin) //Not reached the first (nor last) glyph yet
        {
            boundingBox.left += kerning + advance;
            boundingBox.right += kerning + advance;
        }
        else
        {
            if (ix == ix_begin) //Reach the first glyph, finalize .left
                boundingBox.left += kerning + bearing.x;

            //Increment .right based on whether the last glyph is considered
            //(with an exception for width = 0 character, typically spaces)
            if (ix + 1 < ix_end || glyph.box.width == 0)
                boundingBox.right += kerning + advance;
            else
                boundingBox.right += kerning + bearing.x +
                                     static_cast<float>(glyph.box.width);
        }

        //Update top / bottom bounding box for the glyphs in range
        if (ix >= ix_begin)
        {
            if (-bearing.y < boundingBox.top)
                boundingBox.top = -bearing.y;

            const float glyphHeight = static_cast<float>(glyph.box.height);
            if (glyphHeight - bearing.y > boundingBox.bottom)
                boundingBox.bottom = glyphHeight - bearing.y;
        }
    }

    return boundingBox;
}

void Text::loadGlyphs()
{
    if (m_font != nullptr)
    {
        //Clear current glyphs
        m_glyphs.clear();

        std::vector<unsigned long> utf32CodeUnits = m_text.get_utf32();
        m_glyphs.reserve(utf32CodeUnits.size());

        unsigned long lastChar = 0;
        for (unsigned long utf32char : utf32CodeUnits)
        {
            GlyphEntry glyph;
            if (!m_font->getGlyph(utf32char, glyph))
                continue;

            if (lastChar == 0) //First glyph: No kerning value to compute
                m_glyphs.emplace_back(glyph, 0.f);
            else
            {
                m_glyphs.emplace_back(glyph, m_font->getKerning(lastChar,
                                                                utf32char));
            }

            lastChar = utf32char;
        }

        //Compute size
        BoundingBox boundingBox = getBoundingBox(0, m_glyphs.size());

        //Add the left space twice to the width for proper centering of texts,
        //i.e., (boundingBox.right - boundingBox.left) + 2 * boundingBox.left
        m_width = boundingBox.right + boundingBox.left;
        m_height = boundingBox.bottom - boundingBox.top;
        m_originHeight = -boundingBox.top;

        //Reload GL arrays at next draw pass:
        //to avoid regenerating the font atlas texture multiple times
        //by calling Font::getTexture()
        m_glArraysGenerated = false;
    }
}

void Text::updateGLArrays()
{
    if (m_glArraysGenerated)
        return;

    m_vertices.clear();
    m_texCoords.clear();

    if (m_glyphs.empty() || m_font == nullptr)
        return;

    const GLsizei texWidth = m_font->getTexture().getWidth();
    const GLsizei texHeight = m_font->getTexture().getHeight();

    //Reserve 8 floats per glyph (4 vertices of 2 coordinates each)
    m_vertices.reserve(8 * m_glyphs.size());
    m_texCoords.reserve(8 * m_glyphs.size());

    float xOffset = 0.0, yOffset = m_originHeight;

    for (const GlyphKerning &glyphData : m_glyphs)
    {
        const GlyphEntry &glyph = glyphData.glyph;
        xOffset += glyphData.kerning;

        std::vector<float> vertex = vertexCoords(
            xOffset + glyph.topLeft.x, yOffset - glyph.topLeft.y, glyph.box);

        std::vector<float> texCoord = textureCoords(
            glyph.box, texWidth, texHeight);

        m_vertices.insert(m_vertices.end(), vertex.begin(), vertex.end());
        m_texCoords.insert(m_texCoords.end(), texCoord.begin(), texCoord.end());

        xOffset += glyph.advance.x;
        yOffset += glyph.advance.y;
    }

    m_glArraysGenerated = true;
}

std::vector<float> Text::vertexCoords(float xOffset, float yOffset,
                                      const GlyphBox &box) const
{
    return std::vector<float>
    {
        xOffset, yOffset,
        xOffset, yOffset + static_cast<float>(box.height),
        xOffset + static_cast<float>(box.width),
        yOffset + static_cast<float>(box.height),
        xOffset + static_cast<float>(box.width), yOffset
    };
}

std::vector<float> Text::textureCoords(const GlyphBox &box,
                                       GLsizei texWidth,
                                       GLsizei texHeight) const
{
    return std::vector<float>
    {
        Texture::getTexCoord(box.x, texWidth),
        Texture::getTexCoord(box.y, texHeight),

        Texture::getTexCoord(box.x, texWidth),
        Texture::getTexCoord(box.y + box.height, texHeight),

        Texture::getTexCoord(box.x + box.width, texWidth),
        Texture::getTexCoord(box.y + box.height, texHeight),

        Texture::getTexCoord(box.x + box.width, texWidth),
        Texture::getTexCoord(box.y, texHeight)
    };
}