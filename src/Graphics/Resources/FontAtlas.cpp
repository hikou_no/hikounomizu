/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "FontAtlas.hpp"
#include "Graphics/Resources/Texture.hpp"

#include "Tools/Log.hpp"
#include "Tools/Format.hpp"
#include <cassert>

namespace
{
    constexpr std::size_t GLYPH_MARGIN = 2;
}

FontAtlas::FontAtlas(std::size_t atlasSize) :
m_atlasSize(atlasSize), m_data(m_atlasSize * m_atlasSize, 0)
{

}

const GlyphEntry *FontAtlas::get(FT_ULong character) const
{
    const std::map<FT_ULong, GlyphEntry>::const_iterator it =
        m_glyphs.find(character);

    return (it != m_glyphs.end()) ? &(it->second) : nullptr;
}

const GlyphEntry *FontAtlas::insert(FT_ULong character,
                                    const FT_BitmapGlyph &glyph,
                                    FT_Vector advance)
{
    //The character has not been previously loaded
    assert(m_glyphs.count(character) == 0);

    const FT_Bitmap &bitmap = glyph->bitmap;

    GlyphBox box;
    if (fit(bitmap.width + GLYPH_MARGIN, bitmap.rows + GLYPH_MARGIN, box))
    {
        //Glyph box without margin
        const GlyphBox glyphBox{box.x + GLYPH_MARGIN / 2,
                                box.y + GLYPH_MARGIN / 2,
                                box.width - GLYPH_MARGIN,
                                box.height - GLYPH_MARGIN};

        //Add the glyph box and metrics
        const std::pair<std::map<FT_ULong, GlyphEntry>::iterator, bool> added =
            m_glyphs.emplace(character,
                GlyphEntry{glyphBox,
                       Vector(static_cast<float>(advance.x / 64),
                              static_cast<float>(advance.y / 64)),
                       Vector(static_cast<float>(glyph->left),
                              static_cast<float>(glyph->top))
                });

        //Copy the glyph bitmap to the pixel data
        copy(bitmap, glyphBox);

        return &(added.first->second);
    }

    Log::err(Format::format("Could not insert glyph u{} to font atlas, "
                            "no space left", character));
    return nullptr;
}

bool FontAtlas::load(Texture &texture) const
{
    //Until transition to shaders, generate a new array to pass LUMINANCE data
    std::vector<GLubyte> luminance_alpha(m_data.size() * 2, 255);
    for (std::size_t i = 0; i < m_data.size(); i++)
        luminance_alpha[2 * i + 1] = m_data[i];

    return texture.loadFromMemory(
        static_cast<const GLvoid*>(luminance_alpha.data()),
        GL_RGBA, GL_LUMINANCE_ALPHA,
        static_cast<GLsizei>(m_atlasSize),
        static_cast<GLsizei>(m_atlasSize),
        Texture::MinMagFilter::Nearest);
}

bool FontAtlas::fit(std::size_t width, std::size_t height, GlyphBox &dstFit)
{
    //Browse shelves for a fit
    std::size_t shelf_y = 0;
    for (Shelf &shelf : m_shelves)
    {
        if (shelf.height >= height &&
            m_atlasSize - shelf.width >= width)
        {
            //The box fits in this shelve
            dstFit = GlyphBox{shelf.width, shelf_y, width, height};
            shelf.width += width;
            return true;
        }
        shelf_y += shelf.height;
    }

    //The box cannot fit in this atlas
    if (shelf_y + height > m_atlasSize || width > m_atlasSize)
        return false;

    //Add a shelf to fit the box
    m_shelves.emplace_back(height);
    m_shelves.back().width = width;

    dstFit = GlyphBox{0, shelf_y, width, height};
    return true;
}

void FontAtlas::copy(const FT_Bitmap &bitmap, const GlyphBox &location)
{
    //Check validity of the target area
    assert(location.x + location.width <= m_atlasSize);
    assert(location.y + location.height <= m_atlasSize);
    assert(location.width == bitmap.width);
    assert(location.height == bitmap.rows);

    for (std::size_t j = 0; j < bitmap.rows; j++)
    {
        for (std::size_t i = 0; i < bitmap.width; i++)
        {
            m_data[(location.y + j) * m_atlasSize + (location.x + i)] =
                bitmap.buffer[j * bitmap.width + i];
        }
    }
}