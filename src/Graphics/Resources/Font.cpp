/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Font.hpp"

#include "Tools/Log.hpp"
#include "Tools/BuildValues.hpp" ///< Generated at build time

Font::Font(const FT_Library library, const std::string &relPath, int charSize) :
m_atlas(charSize < 150 ? 1024 : 2048),
///< Arbitrary char size limit to increase the font atlas size
m_textureObsolete(false), m_charSize(charSize), m_kerningAvailable(false)
{
    //Locate absolute path to data
    const std::string path = BuildValues::data(relPath);

    if (FT_New_Face(library, path.c_str(), 0, &m_face) == 0)
    {
        const FT_F26Dot6 ftSize = static_cast<FT_F26Dot6>(charSize * 64);

        FT_Select_Charmap(m_face, ft_encoding_unicode); //Big-Endian UTF-32
        FT_Set_Char_Size(m_face, ftSize, ftSize, 72, 72);

        if (FT_HAS_KERNING(m_face)) //Returns a FT_Bool
            m_kerningAvailable = true;
    }
    else
    {
        Log::err("Could not open font: " + relPath);
        m_face = nullptr;
    }
}

Font::~Font()
{
    if (m_face != nullptr)
        FT_Done_Face(m_face);
}

float Font::getKerning(FT_ULong char1, FT_ULong char2)
{
    if (!m_kerningAvailable)
        return 0.f;

    const FT_UInt glyph1 = FT_Get_Char_Index(m_face, char1),
                  glyph2 = FT_Get_Char_Index(m_face, char2);

    if (glyph1 == 0 || glyph2 == 0)
        return 0.f;

    FT_Vector kerning;
    FT_Get_Kerning(m_face, glyph1, glyph2, FT_KERNING_DEFAULT, &kerning);

    return static_cast<float>(kerning.x / 64);
}

bool Font::getGlyph(FT_ULong character, GlyphEntry &dstGlyph)
{
    //The font does not have a glyph for character
    if (!hasGlyph(character))
        return false;

    //Get the glyph from the atlas or attempt to insert it
    const GlyphEntry *glyphEntry = m_atlas.get(character);
    if (glyphEntry == nullptr)
    {
        glyphEntry = addToAtlas(character);
        if (glyphEntry != nullptr)
            m_textureObsolete = true;
    }

    if (glyphEntry != nullptr)
    {
        dstGlyph = (*glyphEntry);
        return true;
    }

    return false;
}

bool Font::hasGlyph(FT_ULong character) const
{
    return (m_face != nullptr && FT_Get_Char_Index(m_face, character) != 0);
}

int Font::getCharSize() const
{
    return m_charSize;
}

Texture &Font::getTexture() const
{
    if (m_textureObsolete)
    {
        m_atlas.load(m_texture);
        m_textureObsolete = false;
    }

    return m_texture;
}

const GlyphEntry *Font::addToAtlas(FT_ULong character)
{
    //Load character
    FT_Load_Char(m_face, character, FT_LOAD_DEFAULT);

    //Retrieve glyph & information
    FT_Glyph rawGlyph;
    FT_Get_Glyph(m_face->glyph, &rawGlyph);
    FT_Glyph_To_Bitmap(&rawGlyph, FT_RENDER_MODE_NORMAL, 0, 1);

    const FT_BitmapGlyph bitmap = reinterpret_cast<FT_BitmapGlyph>(rawGlyph);
    const FT_GlyphSlot slot = reinterpret_cast<FT_GlyphSlot>(m_face->glyph);

    const GlyphEntry *entry = m_atlas.insert(character, bitmap, slot->advance);

    //Unload glyph
    FT_Done_Glyph(rawGlyph);

    return entry;
}