/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_FONT_ATLAS
#define DEF_FONT_ATLAS

#include "Graphics/GL.hpp"
#include "Structs/Vector.hpp"

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

#include "Glyph.hpp"
#include <vector>
#include <map>
#include <cstddef>

class Texture;

class FontAtlas
{
    public:
        explicit FontAtlas(std::size_t atlasSize = 1024);
        FontAtlas(const FontAtlas &copied) = delete;
        FontAtlas &operator=(const FontAtlas &copied) = delete;

        /// Return the glyph box for \p character or nullptr if none
        const GlyphEntry *get(FT_ULong character) const;

        /// Insert \p glyph for \p character in the atlas
        /// Returns the inserted box or nullptr if it could not be inserted
        const GlyphEntry *insert(FT_ULong character,
                                 const FT_BitmapGlyph &glyph,
                                 FT_Vector advance);

        /// Load the font atlas into \p texture
        bool load(Texture &texture) const;

    private:
        struct Shelf
        {
            explicit Shelf(std::size_t shelfHeight) :
            height(shelfHeight), width(0) {}

            std::size_t height;
            std::size_t width;
        };

        bool fit(std::size_t width, std::size_t height, GlyphBox &dstFit);
        void copy(const FT_Bitmap &bitmap, const GlyphBox &location);

        const std::size_t m_atlasSize;
        std::vector<GLubyte> m_data;
        std::vector<Shelf> m_shelves;
        std::map<FT_ULong, GlyphEntry> m_glyphs;
};

#endif
