/*Copyright (C) 2010-2024 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_DISPLAY
#define DEF_DISPLAY

namespace Display
{
    /// Describes the vertical synchronization mode to apply
    enum class VSyncMode : int
    {
        OnAdaptive = -1, ///< Adaptive vertical synchronization
        On = 1, ///< Regular vertical synchronization
        Off = 0 ///< No vertical synchronization
    };

    /// Describes the display mode to apply
    enum class DisplayMode : int
    {
        Windowed = 0, ///< Regular windowed mode

        Fullscreen = 1, ///< 'True' fullscreen mode, with video mode change

        /// 'Fake' fullscreen mode, with a screen size borderless window
        FullscreenDesktop = 2
    };

    /// Describes a display layout with a mode and a size to apply
    struct DisplayLayout
    {
        DisplayLayout() : DisplayLayout(DisplayMode::Windowed, 1280, 720) {}

        DisplayLayout(DisplayMode displayMode, int w, int h) :
        mode(displayMode), width(w), height(h) {}

        DisplayMode mode;
        int width, height;
    };

    inline int writeVSyncMode(VSyncMode vsyncMode)
    {
        return static_cast<int>(vsyncMode);
    }

    inline VSyncMode readVSyncMode(int vsyncMode)
    {
        if (vsyncMode < -1 || vsyncMode > 1)
            return VSyncMode::OnAdaptive;

        return static_cast<VSyncMode>(vsyncMode);
    }

    inline int writeDisplayMode(DisplayMode displayMode)
    {
        return static_cast<int>(displayMode);
    }

    inline DisplayMode readDisplayMode(int displayMode)
    {
        if (displayMode < 0 || displayMode > 2)
            return DisplayMode::Windowed;

        return static_cast<DisplayMode>(displayMode);
    }
}

#endif
