#!/bin/sh

# Copyright (C) 2010-2024 Duncan Deveaux
#
# This file is part of Hikou no mizu.
#
# Hikou no mizu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Hikou no mizu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.


# filterdata.sh: Filter all data files which are not needed
# to run the Hikou no mizu server. Useful for server-only installations

# Show help and usage message
help()
{
    printf 'Usage: %s [datadir] [outputdir]\n' "$(basename "$0")"
    printf 'Extract the game data that is needed by the Hikou no mizu server\n'
}

datadir="$1"
filterdir="$2"

# Check if the input/output data directory were provided
if [ -z "$datadir" ] || [ -z "$filterdir" ] ; then
    help
    exit 1
fi

# Check if the input data directory exists
if [ ! -d "$datadir" ] ; then
    printf 'The data directory "%s" does not exist.\n' "$datadir"
    exit 1
fi

# Create and populate output directory
mkdir -p "${filterdir}/cfg"
mkdir -p "${filterdir}/gfx/characters/Hana"
mkdir -p "${filterdir}/gfx/characters/Hikou"
mkdir -p "${filterdir}/gfx/characters/Takino"

cp "${datadir}"/cfg/* "${filterdir}/cfg/"
cp "${datadir}/gfx/characters/Hana/data.xml" \
    "${filterdir}/gfx/characters/Hana/"
cp "${datadir}/gfx/characters/Hikou/data.xml" \
    "${filterdir}/gfx/characters/Hikou/"
cp "${datadir}/gfx/characters/Takino/data.xml" \
    "${filterdir}/gfx/characters/Takino/"
